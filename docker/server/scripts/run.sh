#!/bin/bash

set -eux

mkdir -p /dev/net
if [ ! -c /dev/net/tun ]; then
    mknod /dev/net/tun c 10 200
fi

iptables -t nat -N vpndnsmap
iptables -t nat -A PREROUTING -s 10.99.0.0/24 -d 10.224.0.0/15 -j vpndnsmap
# ip route list default
iptables -t nat -A POSTROUTING -s 10.99.0.0/24 -o eth0 -j MASQUERADE

openvpn --config /etc/openvpn/server.conf &

exec air
