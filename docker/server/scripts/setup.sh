#!/bin/bash

set -eux

if [ ! -d "/openvpn/easy-rsa" ]
then
  echo "Create PKI directory"
  make-cadir /openvpn/easy-rsa
  cd /openvpn/easy-rsa

  ./easyrsa init-pki
  ./easyrsa build-ca nopass
  ./easyrsa gen-dh
  ./easyrsa build-server-full server nopass
  # generate key and certificate for 'client' docker container
  ./easyrsa build-client-full client nopass
fi

echo "Initialize application"
cd /app
go run . -init
