# OpenVPN admin interface

DNS proxy и административный интерфейс для OpenVPN сервера. Дает возможность направлять трафик через VPN только для
указанных доменов, сетей, ASN.

## Screenshots

![Login](screenshots/01.png)

![Status](screenshots/02.png)

![Routes](screenshots/03.png)

![Clients](screenshots/04.png)

## Dev окружение

```
cp config.yaml.example config.yaml
docker compose run server /scripts/setup.sh
docker compose up
```

```
cd frontend
npm install
npm run dev
```

## Миграции

Для миграций используется пакет https://github.com/golang-migrate

```sh
go install -tags 'sqlite3' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
```

Миграции применяются автоматически при старте сервиса.

### Добавить новую миграцию

```
migrate create -ext sql -dir assets/migrations migration_name
```

### Применить миграции

```
migrate -path assets/migrations -database sqlite3://data/main.db up
```

### Откатить N миграций

```
migrate -path assets/migrations -database sqlite3://data/main.db down N
```
