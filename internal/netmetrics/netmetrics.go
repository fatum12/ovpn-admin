package netmetrics

import (
	"sync"
)

type Point struct {
	Timestamp int64 `json:"ts"`
	TX        int   `json:"tx"` // Number of bytes transmitted
	RX        int   `json:"rx"` // Number of bytes received
}

type Stat struct {
	fn func() (Point, error)
	q  *queue
	mu sync.RWMutex
}

func NewStat(fn func() (Point, error), limit int) *Stat {
	return &Stat{
		fn: fn,
		q:  newQueue(limit + 1),
	}
}

func (s *Stat) Update() error {
	p, err := s.fn()
	if err != nil {
		return err
	}

	s.mu.Lock()
	if s.q.Size() == s.q.Cap()-1 {
		s.q.Dequeue()
	}
	s.q.Enqueue(p)
	s.mu.Unlock()

	return nil
}

func (s *Stat) Values() []Point {
	s.mu.RLock()
	res := s.q.Values()
	s.mu.RUnlock()
	return res
}
