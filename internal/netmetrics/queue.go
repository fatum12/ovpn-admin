package netmetrics

type queue struct {
	items []Point
	head  int
	tail  int
}

func newQueue(size int) *queue {
	return &queue{
		items: make([]Point, size),
	}
}

func (d *queue) IsEmpty() bool {
	return d.head == d.tail
}

func (d *queue) Size() int {
	if d.tail >= d.head {
		return d.tail - d.head
	}
	return d.Cap() - d.head + d.tail
}

func (d *queue) Cap() int {
	return len(d.items)
}

func (d *queue) Enqueue(p Point) {
	if d.head == (d.tail+1)%d.Cap() {
		panic("queue overflow")
	}
	d.items[d.tail] = p
	d.tail = (d.tail + 1) % d.Cap()
}

func (d *queue) Dequeue() Point {
	if d.IsEmpty() {
		panic("queue underflow")
	}
	p := d.items[d.head]
	d.head = (d.head + 1) % d.Cap()
	return p
}

func (d *queue) Values() []Point {
	res := make([]Point, d.Size())
	if d.tail >= d.head {
		copy(res, d.items[d.head:d.tail])
	} else {
		n := copy(res, d.items[d.head:])
		copy(res[n:], d.items[:d.tail])
	}
	return res
}
