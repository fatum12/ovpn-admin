package errs

type Error struct {
	Status  int // http status
	Code    string
	Message string
	Data    map[string]interface{}
	Cause   error
}

func New(code string) *Error {
	return &Error{
		Status:  list[code].status,
		Code:    code,
		Message: list[code].message,
		Data:    map[string]interface{}{},
	}
}

func (e *Error) Error() string {
	return e.Message
}

func (e *Error) WithCause(err error) *Error {
	e.Cause = err
	return e
}

func (e *Error) WithDataField(key string, value interface{}) *Error {
	e.Data[key] = value
	return e
}

func (e *Error) WithData(data map[string]interface{}) *Error {
	for key, value := range data {
		e.Data[key] = value
	}
	return e
}

func Is(err error, code string) bool {
	e, ok := err.(*Error)
	return ok && e.Code == code
}
