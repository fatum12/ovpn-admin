package errs

import "net/http"

const (
	InternalError = "internal_error"

	InvalidRequest         = "invalid_request"
	EmptyRequestBody       = "empty_request_body"
	ValidationFailed       = "validation_failed"
	InvalidCredentials     = "invalid_credentials"
	AuthenticationRequired = "authentication_required"
	InvalidCSRFToken       = "invalid_csrf_token"

	EntityNotFound   = "entity_not_found"
	ConfigNotExists  = "config_not_exists"
	ShareLinkExpired = "share_link_expired"
)

type errorInfo struct {
	status  int // http status
	message string
}

var list = map[string]errorInfo{
	InternalError: {
		http.StatusInternalServerError,
		"Internal server error",
	},

	InvalidRequest: {
		http.StatusBadRequest,
		"Invalid request",
	},
	EmptyRequestBody: {
		http.StatusBadRequest,
		"Empty request body",
	},
	ValidationFailed: {
		http.StatusUnprocessableEntity,
		"Validation failed",
	},
	InvalidCredentials: {
		http.StatusUnauthorized,
		"Invalid username or password",
	},
	AuthenticationRequired: {
		http.StatusUnauthorized,
		"Authentication required",
	},
	InvalidCSRFToken: {
		http.StatusBadRequest,
		"Invalid CSRF token",
	},

	EntityNotFound: {
		http.StatusNotFound,
		"Entity not found",
	},
	ConfigNotExists: {
		http.StatusNotFound,
		"Config not exists",
	},
	ShareLinkExpired: {
		http.StatusBadRequest,
		"Link has expired",
	},
}
