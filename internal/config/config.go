package config

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"golang.org/x/crypto/bcrypt"
	"net"
	"time"
)

// Config конфиг приложения.
type Config struct {
	Addr    string         `yaml:"addr" binding:"required"`
	Debug   bool           `yaml:"debug"`
	Users   Users          `yaml:"users" binding:"min=1,dive"`
	DB      Database       `yaml:"db"`
	Log     logging.Config `yaml:"log"`
	OpenVPN OpenVPN        `yaml:"openvpn"`
	EasyRSA EasyRSA        `yaml:"easyrsa"`
	DNS     DNSProxy       `yaml:"dns"`
}

type User struct {
	Name     string `binding:"required"`
	Password string `binding:"required"`
}

func (u User) ComparePassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}

type Users []User

func (list Users) FindByName(name string) (User, bool) {
	for _, u := range list {
		if u.Name == name {
			return u, true
		}
	}
	return User{}, false
}

type Database struct {
	Path  string `yaml:"path" binding:"required"`
	Debug bool   `yaml:"debug"`
}

type OpenVPN struct {
	ConfigsDir string `yaml:"configsDir" binding:"required"`
	Host       string `yaml:"host" binding:"required"`
	Port       int    `yaml:"port" binding:"required,gt=0"`
	Cipher     string `yaml:"cipher,omitempty"`
	Auth       string `yaml:"auth,omitempty"`
	Telnet     struct {
		Host string `yaml:"host" binding:"required"`
		Port int    `yaml:"port" binding:"required,gt=0"`
	} `yaml:"telnet"`
	TrafficUpdateSchedule string `yaml:"trafficUpdateSchedule,omitempty"`
}

type EasyRSA struct {
	BaseDir   string `yaml:"baseDir" binding:"required"`
	KeyExpire int    `yaml:"keyExpire,omitempty" binding:"omitempty,gt=0"` // In how many days should certificate expire
}

type DNSProxy struct {
	IP            net.IP        `yaml:"ip" binding:"required"`
	Port          int           `yaml:"port,omitempty" binding:"omitempty,gt=0"`
	Upstreams     []string      `yaml:"upstreams" binding:"required,dive,required"`
	Timeout       time.Duration `yaml:"timeout,omitempty"`
	MappingRange  string        `yaml:"mappingRange" binding:"cidrv4"`
	MappingMaxTTL uint32        `yaml:"mappingMaxTTL,omitempty"` // in seconds

	IPTablesPath      string `yaml:"iptablesPath,omitempty"`
	IPTablesTableName string `yaml:"iptablesTableName" binding:"required"`

	CacheEnabled bool `yaml:"cacheEnabled,omitempty"`
	CacheSize    int  `yaml:"cacheSize,omitempty"` // in bytes
}
