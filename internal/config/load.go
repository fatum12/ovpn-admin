package config

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/utils"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/yaml.v3"
	"os"
)

var cfg Config

// Load загружает конфиг из указанного файла и валидирует его.
func Load(path string, validate *validator.Validate) (Config, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return cfg, errors.Wrapf(err, "read config %s", path)
	}
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		return cfg, errors.Wrap(err, "unmarshal yaml")
	}

	if err = validate.Struct(cfg); err != nil {
		return cfg, errors.Wrap(err, "validate config")
	}

	var needSave bool
	for i, user := range cfg.Users {
		if !isHashed(user.Password) {
			var hash []byte
			hash, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
			if err != nil {
				return cfg, errors.Wrap(err, "hash password for user "+user.Name)
			}
			cfg.Users[i].Password = string(hash)
			needSave = true
		}
	}

	if needSave {
		data, err = yaml.Marshal(&cfg)
		if err != nil {
			return cfg, errors.Wrap(err, "marshal yaml")
		}
		err = utils.WriteFile(path, data)
	}

	return cfg, err
}

func isHashed(val string) bool {
	_, err := bcrypt.Cost([]byte(val))
	return err == nil
}
