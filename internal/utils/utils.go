package utils

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

func ParseSubnet(s string) (net.IP, net.IP, error) {
	if strings.ContainsRune(s, '/') {
		_, network, err := net.ParseCIDR(s)
		if err != nil {
			return nil, nil, errors.Wrap(err, "parse subnet")
		}
		return network.IP, net.IP(network.Mask), nil
	}
	ip := net.ParseIP(s)
	if ip == nil {
		return nil, nil, errors.Errorf("invalid ip address: %s", s)
	}
	if ip.To4() != nil {
		ip = ip.To4()
	}
	size := len(ip) * 8
	mask := net.CIDRMask(size, size)
	return ip, net.IP(mask), nil
}

func NormalizeSubnet(s string) string {
	parts := strings.SplitN(s, "/", 2)
	if len(parts) == 2 && parts[1] == "32" {
		return parts[0]
	}
	return s
}

func NormalizeASN(asn string) string {
	return strings.ToUpper(asn)
}

func NormalizeHost(host string) string {
	host = strings.ToLower(host)
	host = strings.Trim(host, " ./\\")
	host = strings.TrimPrefix(host, "https://")
	host = strings.TrimPrefix(host, "http://")
	return host
}

type bgpviewResponse struct {
	Data struct {
		Prefixes []bgpviewPrefix `json:"ipv4_prefixes"`
	} `json:"data"`
}

type bgpviewPrefix struct {
	Prefix string `json:"prefix"`
}

func ASNPrefixes(ctx context.Context, asn string) ([]string, error) {
	var result []string
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://api.bgpview.io/asn/"+asn+"/prefixes", nil)
	if err != nil {
		return result, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return result, err
	}
	defer func() { _ = resp.Body.Close() }()

	status := resp.StatusCode
	if status != http.StatusOK {
		return result, errors.Errorf("unexpected bgpview response status: %d", status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return result, errors.Wrap(err, "read bgpview response")
	}
	var data bgpviewResponse
	if err = json.Unmarshal(body, &data); err != nil {
		return result, errors.Wrap(err, "unmarshal bgpview response")
	}

	result = make([]string, len(data.Data.Prefixes))
	for i, p := range data.Data.Prefixes {
		result[i] = p.Prefix
	}
	return result, nil
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func WriteFile(filename string, data []byte) (err error) {
	dir := filepath.Dir(filename)
	if !FileExists(dir) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return errors.Wrap(err, "make directory "+dir)
		}
	}
	err = os.WriteFile(filename, data, 0644)
	return errors.Wrap(err, "write file "+filename)
}

// WaitFn запускает параллельное выполнение функций и ждет их завершения.
func WaitFn(fn ...func()) {
	var wg sync.WaitGroup
	wg.Add(len(fn))
	for _, f := range fn {
		go func(f func()) {
			defer wg.Done()
			f()
		}(f)
	}
	wg.Wait()
}
