package utils

import (
	"net/http"
	"path"
)

func RelativeFS(fs http.FileSystem, root string) http.FileSystem {
	return relativeFS{
		fs:   fs,
		root: root,
	}
}

type relativeFS struct {
	fs   http.FileSystem
	root string
}

func (fs relativeFS) Open(name string) (http.File, error) {
	name = path.Join(fs.root, name)
	return fs.fs.Open(name)
}
