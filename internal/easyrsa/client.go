package easyrsa

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/config"
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"bufio"
	"context"
	"github.com/pkg/errors"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var ErrCertificateNotFound = errors.New("certificate not found")

type Client struct {
	cfg    config.EasyRSA
	pkiDir string
}

func NewClient(cfg config.EasyRSA) *Client {
	if cfg.KeyExpire == 0 {
		cfg.KeyExpire = 365
	}

	return &Client{
		cfg:    cfg,
		pkiDir: filepath.Join(cfg.BaseDir, "pki"),
	}
}

// Certificates возвращает список выпущенных сертификатов (действующих и отозванных).
func (c *Client) Certificates() ([]*Certificate, error) {
	filePath := filepath.Join(c.pkiDir, "index.txt")
	file, err := os.Open(filePath)
	if err != nil {
		return nil, errors.Wrap(err, "open certificates db")
	}
	defer func() { _ = file.Close() }()

	var result []*Certificate
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "\t")
		if len(parts) < 6 {
			return nil, errors.Errorf("easyrsa: can't parse line %q", line)
		}

		certDesc := parseCertDescription(parts[5])
		cert := Certificate{
			Name:  certDesc["CN"],
			Valid: parts[0] == "V",
			Index: parts[3],
		}
		cert.ExpireAt, err = parseCertDate(parts[1])
		if err != nil {
			return nil, err
		}
		if parts[2] != "" {
			var t time.Time
			t, err = parseCertDate(parts[2])
			if err != nil {
				return nil, err
			}
			cert.Revoked = &t
		}
		result = append(result, &cert)
	}
	return result, errors.WithStack(scanner.Err())
}

// CertificateByName возвращает информацию о выпущенном сертификате по имени.
func (c *Client) CertificateByName(name string) (*Certificate, error) {
	certs, err := c.Certificates()
	if err != nil {
		return nil, err
	}
	var found *Certificate
	for _, cert := range certs {
		// ищем последний сертификат с таким именем
		if cert.Name == name {
			found = cert
		}
	}
	if found == nil {
		return nil, ErrCertificateNotFound
	}
	return found, nil
}

// CreateCertificate выпускает новый клиентский сертификат.
func (c *Client) CreateCertificate(ctx context.Context, name, password string) error {
	cmd := c.newCommand(ctx, "--days="+strconv.Itoa(c.cfg.KeyExpire), "build-client-full", name)
	if password != "" {
		cmd.Env = append(cmd.Env, "EASYRSA_PASSOUT=pass:"+password)
	} else {
		cmd.Args = append(cmd.Args, "nopass")
	}
	return c.execCommand(ctx, cmd)
}

// RevokeCertificate отзывает сертификат.
func (c *Client) RevokeCertificate(ctx context.Context, name string) error {
	cmd := c.newCommand(ctx, "revoke", name)
	cmd.Stdin = strings.NewReader("yes\n")
	return c.execCommand(ctx, cmd)
}

// ReadCertificate возвращает клиентский сертификат по имени.
func (c *Client) ReadCertificate(name string) ([]byte, error) {
	res, err := os.ReadFile(c.certificatePath(name))
	return res, errors.Wrap(err, "read cert "+name)
}

func (c *Client) certificatePath(name string) string {
	return filepath.Join(c.pkiDir, "issued", name+".crt")
}

// ReadAuthorityCertificate возвращает сертификат удостоверяющего центра.
func (c *Client) ReadAuthorityCertificate() ([]byte, error) {
	res, err := os.ReadFile(filepath.Join(c.pkiDir, "ca.crt"))
	return res, errors.Wrap(err, "read ca cert")
}

// ReadPrivateKey возвращает клиентский ключ.
func (c *Client) ReadPrivateKey(name string) ([]byte, error) {
	res, err := os.ReadFile(c.privateKeyPath(name))
	return res, errors.Wrap(err, "read private key "+name)
}

func (c *Client) privateKeyPath(name string) string {
	return filepath.Join(c.pkiDir, "private", name+".key")
}

func (c *Client) execCommand(ctx context.Context, cmd *exec.Cmd) error {
	logger := logging.CtxLogger(ctx)
	logger.Debug("exec command: ", cmd.String())
	out, err := cmd.CombinedOutput()
	logger.Debugf("exec output: %s", out)
	return errors.Wrap(err, "exec easyrsa command")
}

func (c *Client) newCommand(ctx context.Context, args ...string) *exec.Cmd {
	cmd := exec.CommandContext(ctx, "./easyrsa", args...)
	cmd.Dir = c.cfg.BaseDir
	return cmd
}

func parseCertDescription(s string) map[string]string {
	params := strings.Split(strings.Trim(s, "/"), "/")
	result := make(map[string]string, len(params))
	for _, param := range params {
		parts := strings.SplitN(param, "=", 2)
		result[parts[0]] = parts[1]
	}
	return result
}

func parseCertDate(date string) (time.Time, error) {
	d, err := time.Parse("060102150405Z", date)
	return d, errors.Wrap(err, "parse cert date")
}
