package easyrsa

import "time"

// Certificate хранит информацию о сертификате в базе Easy-RSA.
type Certificate struct {
	Name     string     `json:"name"`
	Valid    bool       `json:"valid"`
	Index    string     `json:"index"`
	ExpireAt time.Time  `json:"expireAt"`
	Revoked  *time.Time `json:"revoked,omitempty"`
}
