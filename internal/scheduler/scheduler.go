package scheduler

import (
	"context"
	"github.com/pkg/errors"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
)

// Engine выполняет периодические задачи.
type Engine struct {
	logger logrus.FieldLogger
	cron   *cron.Cron
}

func New(logger logrus.FieldLogger) *Engine {
	log := cron.PrintfLogger(logger)
	c := cron.New(
		cron.WithLogger(log),
		cron.WithChain(cron.SkipIfStillRunning(log)),
	)
	return &Engine{
		logger: logger,
		cron:   c,
	}
}

func (e *Engine) Start() {
	e.logger.Info("starting cron...")
	e.cron.Start()
	e.logger.Info("cron started")
}

func (e *Engine) Stop(timeoutCtx context.Context) {
	e.logger.Info("stopping cron...")
	ctx := e.cron.Stop()
	select {
	case <-ctx.Done():
		e.logger.Info("cron stopped")
	case <-timeoutCtx.Done():
		e.logger.Warn("cron stop timeout reached")
	}
}

func (e *Engine) AddFunc(name, schedule string, f func() error) error {
	e.logger.Infof("register job %s: %s", name, schedule)
	_, err := e.cron.AddFunc(schedule, e.wrapFunc(name, f))
	return errors.WithStack(err)
}

func (e *Engine) wrapFunc(name string, f func() error) func() {
	return func() {
		logger := e.logger.WithField("job", name)
		logger.Debug("starting job")
		err := f()
		if err != nil {
			logger.Errorf("%+v", err)
		}
		logger.Debug("job finished")
	}
}
