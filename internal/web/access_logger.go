package web

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
	"time"
)

func AccessLogger(c *gin.Context) {
	startTime := time.Now()
	entry := logging.CtxLogger(c.Request.Context()).WithFields(logrus.Fields{
		"method": c.Request.Method,
		"url":    c.Request.URL.Path,
		"ip":     c.ClientIP(),
	})

	entry.WithFields(logrus.Fields{
		"headers": sanitizeHeaders(c.Request.Header),
	}).Info("request")

	c.Next()

	entry.WithFields(logrus.Fields{
		"status":   c.Writer.Status(),
		"headers":  sanitizeHeaders(c.Writer.Header()),
		"duration": logging.FormatDuration(time.Since(startTime)),
	}).Info("response")
}

const replacer = "***"

func sanitizeHeaders(src http.Header) http.Header {
	dst := make(map[string][]string, len(src))
	for key, value := range src {
		switch strings.ToLower(key) {
		case "cookie", "set-cookie":
			dst[key] = []string{replacer}
		default:
			dst[key] = value
		}
	}
	return dst
}
