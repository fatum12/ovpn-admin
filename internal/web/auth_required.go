package web

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"github.com/gin-gonic/gin"
)

func AuthRequired(c *gin.Context) {
	if !webcontext.LoggedIn(c) {
		webcontext.RedirectToLogin(c)
		c.Abort()
	}
}
