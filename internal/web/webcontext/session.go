package webcontext

import (
	"bitbucket.org/fatum12/ginx/sessions"
	"encoding/gob"
	"github.com/gin-gonic/gin"
)

const (
	sessionKeyUsername   = "username"
	sessionKeyFormErrors = "form_errors"
)

func init() {
	gob.Register(map[string]string{})
}

func SetUsername(c *gin.Context, username string) error {
	if err := sessions.RenewToken(c); err != nil {
		return err
	}
	sessions.Put(c, sessionKeyUsername, username)
	return nil
}

func Username(c *gin.Context) string {
	username, _ := sessions.Get(c, sessionKeyUsername).(string)
	return username
}

func LoggedIn(c *gin.Context) bool {
	return Username(c) != ""
}

func Logout(c *gin.Context) error {
	if err := sessions.RenewToken(c); err != nil {
		return err
	}
	sessions.Remove(c, sessionKeyUsername)
	return nil
}

func SetFormErrors(c *gin.Context, formErrs map[string]string) {
	sessions.Put(c, sessionKeyFormErrors, formErrs)
}

func PopFormErrors(c *gin.Context) map[string]string {
	formErrs, _ := sessions.Pop(c, sessionKeyFormErrors).(map[string]string)
	if formErrs == nil {
		formErrs = make(map[string]string)
	}
	return formErrs
}
