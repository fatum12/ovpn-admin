package webcontext

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ginx"
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"path"
)

func RedirectToLogin(c *gin.Context) {
	ginertia.Location(c, "/login")
}

func RedirectToHome(c *gin.Context) {
	ginx.Redirect(c, "/")
}

func Logger(c *gin.Context) logrus.FieldLogger {
	return logging.CtxLogger(c.Request.Context())
}

func Path(params ...any) string {
	elements := make([]string, len(params))
	for i, p := range params {
		if str, ok := p.(string); ok {
			elements[i] = str
		} else {
			elements[i] = fmt.Sprint(p)
		}
	}
	return path.Join(elements...)
}
