package webcontext

import (
	"bitbucket.org/fatum12/ginx/sessions"
	"encoding/gob"
	"github.com/gin-gonic/gin"
)

type FlashType string

const (
	FlashTypeSuccess FlashType = "success"
	FlashTypeError   FlashType = "danger"
)

func init() {
	gob.Register(&FlashMessage{})
}

type FlashMessage struct {
	Type FlashType `json:"type"`
	Text string    `json:"text"`
}

func (fm *FlashMessage) AsSuccess() *FlashMessage {
	fm.Type = FlashTypeSuccess
	return fm
}

func (fm *FlashMessage) AsError() *FlashMessage {
	fm.Type = FlashTypeError
	return fm
}

const sessionKeyFlash = "flash"

// Flash добавляет в сессию flash сообщение.
func Flash(c *gin.Context, message string) *FlashMessage {
	fm := &FlashMessage{
		Type: FlashTypeSuccess,
		Text: message,
	}
	sessions.Put(c, sessionKeyFlash, fm)
	return fm
}

// PopFlash извлекает flash сообщение из сессии.
func PopFlash(c *gin.Context) *FlashMessage {
	val, _ := sessions.Pop(c, sessionKeyFlash).(*FlashMessage)
	return val
}
