package handlers

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ginx"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"github.com/gin-gonic/gin"
)

func (h *handlers) loginIndex(c *gin.Context) error {
	if webcontext.LoggedIn(c) {
		webcontext.RedirectToHome(c)
		return nil
	}

	ginertia.Render(c, "Login", nil)
	return nil
}

type loginRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (h *handlers) login(c *gin.Context) (err error) {
	var req loginRequest
	if err = h.bindRequest(c, &req); err != nil {
		return err
	}

	user, ok := h.Config.Users.FindByName(req.Username)
	if !ok {
		webcontext.Logger(c).Errorf("username '%s' not found", req.Username)
		webcontext.Flash(c, "Invalid username or password").AsError()
		ginx.RedirectBack(c)
		return nil
	}
	if err = user.ComparePassword(req.Password); err != nil {
		webcontext.Logger(c).Error("password mismatch: ", err)
		webcontext.Flash(c, "Invalid username or password").AsError()
		ginx.RedirectBack(c)
		return nil
	}

	if err = webcontext.SetUsername(c, user.Name); err != nil {
		return err
	}
	webcontext.RedirectToHome(c)
	return nil
}

func (h *handlers) logout(c *gin.Context) (err error) {
	if err = webcontext.Logout(c); err != nil {
		return err
	}
	webcontext.RedirectToLogin(c)
	return nil
}
