package handlers

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ovpn-admin/internal/openvpn"
	"github.com/gin-gonic/gin"
)

func (h *handlers) statusIndex(c *gin.Context) error {
	ctx := c.Request.Context()
	props := make(map[string]any, 4)
	err := h.OpenVPN.WithTelnetConnection(ctx, func(service *openvpn.Service) (e error) {
		props["serverVersion"], e = service.Version(ctx)
		if e != nil {
			return e
		}
		props["stats"], e = service.Stats(ctx)
		if e != nil {
			return e
		}
		props["connections"], e = service.Connections(ctx)
		return e
	})
	if err != nil {
		return err
	}

	props["traffic"] = h.NetMetrics.Values()

	ginertia.Render(c, "Status", props)
	return nil
}
