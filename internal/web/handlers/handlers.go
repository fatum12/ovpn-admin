package handlers

import (
	"bitbucket.org/fatum12/ginx"
	"bitbucket.org/fatum12/ovpn-admin/internal/di"
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/validation"
	"bitbucket.org/fatum12/ovpn-admin/internal/web"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"io"
)

type handlers struct {
	*di.Container
}

func (*handlers) bindRequest(c *gin.Context, req any) error {
	// TODO: trim перед валидацией
	err := c.ShouldBindJSON(req)
	if errors.Is(err, io.EOF) {
		return errs.New(errs.EmptyRequestBody)
	}
	return err
}

func Register(router gin.IRouter, deps *di.Container) {
	h := handlers{deps}

	authGroup := router.Group("", web.AuthRequired)
	{
		authGroup.GET("/", ginx.Handler(h.statusIndex))

		authGroup.GET("/routes", ginx.Handler(h.routesIndex))
		authGroup.POST("/routes", ginx.Handler(h.createRoute))
		authGroup.POST("/routes/config/update", ginx.Handler(h.updateRoutesConfig))
		authGroup.POST("/routes/reorder", ginx.Handler(h.reorderRoute))
		authGroup.DELETE("/routes/:id", ginx.Handler(h.deleteRoute))
		authGroup.POST("/routes/:id/restore", ginx.Handler(h.restoreRoute))
		authGroup.POST("/routes/clear-cache", ginx.Handler(h.clearRoutesCache))

		authGroup.POST("/connections/:name/kill", ginx.Handler(h.killConnection))

		authGroup.GET("/clients", ginx.Handler(h.clientsIndex))
		authGroup.POST("/clients", ginx.Handler(h.createClient))
		authGroup.GET("/clients/:name", ginx.Handler(h.clientOptionsIndex))
		authGroup.POST("/clients/:name/revoke-cert", ginx.Handler(h.revokeClientCertificate))
		authGroup.POST("/clients/:name/reissue-cert", ginx.Handler(h.reissueClientCertificate))
		authGroup.GET("/clients/:name/connect-config/download", ginx.Handler(h.downloadClientConfig))
		authGroup.POST("/clients/:name", ginx.Handler(h.saveClientOptions))
		authGroup.POST("/clients/:name/share", ginx.Handler(h.createShareLink))

		authGroup.GET("/share-links/:id", ginx.Handler(h.shareLinkIndex))
		authGroup.DELETE("/share-links/:id", ginx.Handler(h.deleteShareLink))
	}

	router.GET("/login", ginx.Handler(h.loginIndex))
	router.POST("/auth/login", ginx.Handler(h.login))
	router.POST("/auth/logout", ginx.Handler(h.logout))

	router.GET("/share/:token", ginx.Handler(h.downloadClientConfigByAccessToken))

	deps.Validator.RegisterStructValidation(validation.SelfValidator(deps), createRouteRequest{})
}
