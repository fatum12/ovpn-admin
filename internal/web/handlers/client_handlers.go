package handlers

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ginx"
	"bitbucket.org/fatum12/ovpn-admin/internal/easyrsa"
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Client struct {
	*models.Client
	Cert *easyrsa.Certificate `json:"cert"`
}

func (h *handlers) clientsIndex(c *gin.Context) error {
	clients, err := h.Store.GetClients()
	if err != nil {
		return err
	}

	certs, err := h.EasyRSA.Certificates()
	if err != nil {
		return err
	}
	certsByName := make(map[string]*easyrsa.Certificate)
	for _, cert := range certs {
		certsByName[cert.Name] = cert
	}

	resp := make([]Client, len(clients))
	for i, c := range clients {
		resp[i] = Client{
			Client: c,
			Cert:   certsByName[c.Name],
		}
	}
	ginertia.Render(c, "Clients/Index", map[string]any{
		"clients": resp,
	})
	return nil
}

type createClientRequest struct {
	Name     string `json:"name" binding:"required,cert_name,unique_cert_name"`
	Password string `json:"password" binding:"omitempty,min=6,max=30"`
}

func (h *handlers) createClient(c *gin.Context) (err error) {
	var req createClientRequest
	if err = h.bindRequest(c, &req); err != nil {
		return err
	}
	err = h.EasyRSA.CreateCertificate(c.Request.Context(), req.Name, req.Password)
	if err != nil {
		return err
	}

	client := &models.Client{
		Name: req.Name,
	}
	if err = h.Store.CreateClient(client); err != nil {
		return err
	}

	webcontext.Flash(c, "Client created")
	ginx.RedirectBack(c)
	return nil
}

func (h *handlers) revokeClientCertificate(c *gin.Context) error {
	name := c.Param("name")
	_, err := h.Store.GetClientByName(name)
	if err != nil {
		return err
	}

	if err = h.EasyRSA.RevokeCertificate(c.Request.Context(), name); err != nil {
		return err
	}

	webcontext.Flash(c, "Certificate revoked")
	ginx.RedirectBack(c)
	return nil
}

type reissueClientCertificateRequest struct {
	Password string `json:"password" binding:"omitempty,min=6,max=30"`
}

func (h *handlers) reissueClientCertificate(c *gin.Context) error {
	name := c.Param("name")
	_, err := h.Store.GetClientByName(name)
	if err != nil {
		return err
	}

	var req reissueClientCertificateRequest
	if err = h.bindRequest(c, &req); err != nil {
		return err
	}

	if err = h.EasyRSA.RevokeCertificate(c.Request.Context(), name); err != nil {
		return err
	}
	err = h.EasyRSA.CreateCertificate(c.Request.Context(), name, req.Password)
	if err != nil {
		return err
	}

	webcontext.Flash(c, "Certificate reissued")
	ginx.RedirectBack(c)
	return nil
}

func (h *handlers) downloadClientConfig(c *gin.Context) error {
	name := c.Param("name")
	return h.sendClientConfig(c, name)
}

// https://github.com/OpenVPN/openvpn3/blob/master/doc/webauth.md#openvpnimport-profile-url
func (h *handlers) downloadClientConfigByAccessToken(c *gin.Context) error {
	token := c.Param("token")
	shareLink, err := h.Store.GetShareLinkByToken(c.Request.Context(), token)
	if err != nil {
		return err
	}
	if shareLink.IsExpired() {
		webcontext.Logger(c).Info("share link expired")
		return errs.New(errs.ShareLinkExpired)
	}
	if shareLink.IsProcessed() {
		webcontext.Logger(c).Info("share link already processed")
		return errs.New(errs.ShareLinkExpired)
	}
	client, err := h.Store.GetClientByID(c.Request.Context(), shareLink.ClientID)
	if err != nil {
		return err
	}
	if err = h.Store.MarkShareLinkAsProcessed(c.Request.Context(), shareLink.ID); err != nil {
		return err
	}
	if err = h.sendClientConfig(c, client.Name); err != nil {
		return err
	}
	return nil
}

func (h *handlers) sendClientConfig(c *gin.Context, clientName string) error {
	cert, err := h.EasyRSA.ReadCertificate(clientName)
	if err != nil {
		return err
	}
	key, err := h.EasyRSA.ReadPrivateKey(clientName)
	if err != nil {
		return err
	}
	ca, err := h.EasyRSA.ReadAuthorityCertificate()
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	err = h.OpenVPN.WriteClientConnectConfig(ca, cert, key, &buf)
	if err != nil {
		return err
	}

	fileName := fmt.Sprintf("client_%s.ovpn", clientName)
	c.DataFromReader(http.StatusOK, int64(buf.Len()), "application/x-openvpn-profile", &buf, map[string]string{
		"Content-Disposition": `attachment; filename="` + fileName + `"`,
	})
	return nil
}

func (h *handlers) clientOptionsIndex(c *gin.Context) error {
	name := c.Param("name")
	client, err := h.Store.GetClientByName(name)
	if err != nil {
		return err
	}

	ginertia.Render(c, "Clients/Options", map[string]any{
		"client": client,
	})
	return nil
}

type saveClientOptionsRequest struct {
	RoutingAllTraffic bool   `json:"routingAllTraffic"`
	PrimaryDNS        string `json:"primaryDns" binding:"omitempty,ip"`
	SecondaryDNS      string `json:"secondaryDns" binding:"omitempty,ip"`
}

func (h *handlers) saveClientOptions(c *gin.Context) (err error) {
	var req saveClientOptionsRequest
	if err = h.bindRequest(c, &req); err != nil {
		return err
	}

	name := c.Param("name")
	client, err := h.Store.GetClientByName(name)
	if err != nil {
		return err
	}
	client.RoutingAllTraffic = req.RoutingAllTraffic
	client.PrimaryDNS = req.PrimaryDNS
	client.SecondaryDNS = req.SecondaryDNS
	if err = h.Store.SaveClient(client); err != nil {
		return err
	}

	if err = h.OpenVPN.UpdateClientConfig(client); err != nil {
		return err
	}

	webcontext.Flash(c, "Configuration saved")
	ginx.RedirectBack(c)
	return nil
}
