package handlers

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ginx"
	"bitbucket.org/fatum12/ovpn-admin/internal/di"
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"bitbucket.org/fatum12/ovpn-admin/internal/openvpn"
	"bitbucket.org/fatum12/ovpn-admin/internal/store"
	"bitbucket.org/fatum12/ovpn-admin/internal/utils"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"github.com/AlekSi/pointer"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func (h *handlers) routesIndex(c *gin.Context) error {
	routes, err := h.Store.GetRoutes()
	if err != nil {
		return err
	}
	ginertia.Render(c, "Routes/Index", map[string]any{
		"routes": routes,
	})
	return nil
}

type createRouteRequest struct {
	Type        models.RouteType `json:"type" binding:"required,route_type"`
	IP          string           `json:"ip" binding:"omitempty,ip|cidr"`
	Host        string           `json:"host" binding:"omitempty,domain_name"`
	ASN         string           `json:"asn" binding:"omitempty,asn"`
	Description string           `json:"description" binding:"max=255"`
}

func (req createRouteRequest) Validate(sl validator.StructLevel, deps *di.Container) {
	switch req.Type {
	case models.RouteTypeIP:
		if req.IP == "" {
			sl.ReportError(req.IP, "ip", "IP", "required", "")
			return
		}
		_, err := deps.Store.GetRoute(store.RouteIP(utils.NormalizeSubnet(req.IP)))
		if !errs.Is(err, errs.EntityNotFound) {
			sl.ReportError(req.IP, "ip", "IP", "unique_route", "")
		}
	case models.RouteTypeHost:
		if req.Host == "" {
			sl.ReportError(req.Host, "host", "Host", "required", "")
			return
		}
		_, err := deps.Store.GetRoute(store.RouteHost(utils.NormalizeHost(req.Host)))
		if !errs.Is(err, errs.EntityNotFound) {
			sl.ReportError(req.Host, "host", "Host", "unique_route", "")
		}
	case models.RouteTypeASN:
		if req.ASN == "" {
			sl.ReportError(req.ASN, "asn", "ASN", "required", "")
			return
		}
		_, err := deps.Store.GetRoute(store.RouteASN(utils.NormalizeASN(req.ASN)))
		if !errs.Is(err, errs.EntityNotFound) {
			sl.ReportError(req.ASN, "asn", "ASN", "unique_route", "")
		}
	}
}

func (h *handlers) createRoute(c *gin.Context) error {
	var req createRouteRequest
	if err := h.bindRequest(c, &req); err != nil {
		return err
	}

	route := &models.Route{
		Description: pointer.ToStringOrNil(req.Description),
	}
	switch req.Type {
	case models.RouteTypeIP:
		route.SetIP(req.IP)
	case models.RouteTypeHost:
		route.SetHost(req.Host)
	case models.RouteTypeASN:
		route.SetASN(req.ASN)
	}
	if err := h.Store.CreateRoute(route); err != nil {
		return err
	}

	ginx.RedirectBack(c)
	return nil
}

func (h *handlers) deleteRoute(c *gin.Context) error {
	id, err := ginx.ParamInt(c, "id")
	if err != nil {
		return errs.New(errs.InvalidRequest).WithCause(err)
	}
	if err = h.Store.SoftDeleteRoute(id); err != nil {
		return err
	}

	ginx.RedirectBack(c)
	return nil
}

func (h *handlers) restoreRoute(c *gin.Context) error {
	id, err := ginx.ParamInt(c, "id")
	if err != nil {
		return errs.New(errs.InvalidRequest).WithCause(err)
	}
	if err = h.Store.RestoreRoute(id); err != nil {
		return err
	}

	ginx.RedirectBack(c)
	return nil
}

func (h *handlers) updateRoutesConfig(c *gin.Context) (err error) {
	routes, err := h.Store.GetRoutes(store.DeletedRoutes(false))
	if err != nil {
		return err
	}

	var domains []string
	for _, r := range routes {
		if r.Type.IsHost() {
			domains = append(domains, *r.Host)
		}
	}
	h.DNSProxy.SetReservedDomains(domains)

	if err = h.OpenVPN.UpdateRoutesConfig(c.Request.Context(), openvpn.DefaultRoutesConfigName, routes); err != nil {
		return err
	}
	if err = h.OpenVPN.UpdateDefaultClientConfig(); err != nil {
		return err
	}
	if err = h.Store.DeleteMarkedRoutes(); err != nil {
		return err
	}
	if err = h.Store.MarkRoutesAsProcessed(); err != nil {
		return err
	}

	webcontext.Flash(c, "Config updated")
	ginx.RedirectBack(c)
	return nil
}

type reorderRouteRequest struct {
	ID     int `json:"id"`
	PrevID int `json:"prev_id"`
	NextID int `json:"next_id"`
}

func (h *handlers) reorderRoute(c *gin.Context) error {
	var req reorderRouteRequest
	if err := h.bindRequest(c, &req); err != nil {
		return err
	}
	if err := h.Store.ReorderRoute(req.ID, req.PrevID, req.NextID); err != nil {
		return err
	}
	ginx.RedirectBack(c)
	return nil
}

func (h *handlers) clearRoutesCache(c *gin.Context) error {
	if err := h.DNSProxy.ClearCache(); err != nil {
		return err
	}
	webcontext.Flash(c, "Cache cleared")
	ginx.RedirectBack(c)
	return nil
}
