package handlers

import (
	"bitbucket.org/fatum12/ginx"
	"github.com/gin-gonic/gin"
)

func (h *handlers) killConnection(c *gin.Context) error {
	name := c.Param("name")
	if err := h.OpenVPN.KillConnection(c.Request.Context(), name); err != nil {
		return err
	}
	ginx.RedirectBack(c)
	return nil
}
