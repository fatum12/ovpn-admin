package handlers

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ginx"
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"github.com/gin-gonic/gin"
)

func (h *handlers) createShareLink(c *gin.Context) error {
	clientName := c.Param("name")
	client, err := h.Store.GetClientByName(clientName)
	if err != nil {
		return err
	}

	link := &models.ShareLink{
		ClientID: client.ID,
	}
	if err = h.Store.CreateShareLink(c.Request.Context(), link); err != nil {
		return err
	}

	ginx.Redirect(c, webcontext.Path("/share-links", link.ID))
	return nil
}

func (h *handlers) shareLinkIndex(c *gin.Context) error {
	id, err := ginx.ParamInt(c, "id")
	if err != nil {
		return errs.New(errs.InvalidRequest).WithCause(err)
	}
	shareLink, err := h.Store.GetShareLinkByID(c.Request.Context(), id)
	if err != nil {
		return err
	}

	client, err := h.Store.GetClientByID(c.Request.Context(), shareLink.ClientID)
	if err != nil {
		return err
	}

	// only https allowed
	link := "https://" + webcontext.Path(c.Request.Host, "share", shareLink.Token)
	ginertia.Render(c, "ShareLinks/Show", map[string]any{
		"clientName": client.Name,
		"link":       link,
		"importLink": "openvpn://import-profile/" + link,
		"linkID":     shareLink.ID,
		"expireAt":   shareLink.ExpireAt,
	})
	return nil
}

func (h *handlers) deleteShareLink(c *gin.Context) error {
	id, err := ginx.ParamInt(c, "id")
	if err != nil {
		return errs.New(errs.InvalidRequest).WithCause(err)
	}
	if err = h.Store.DeleteShareLink(c.Request.Context(), id); err != nil {
		return err
	}
	webcontext.Flash(c, "Share link deleted")
	ginx.Redirect(c, "/clients")
	return nil
}
