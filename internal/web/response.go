package web

type ErrorResponse struct {
	Code    string      `json:"code"`              // код ошибки
	Message string      `json:"message,omitempty"` // описание ошибки
	Detail  string      `json:"detail,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}
