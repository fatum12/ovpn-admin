package web

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"github.com/gin-gonic/gin"
)

// ShareProps прокидывает глобальные свойства в шаблон.
func ShareProps(c *gin.Context) {
	ginertia.ShareProp(c, "errors", webcontext.PopFormErrors(c))

	ginertia.ShareProp(c, "flash", webcontext.PopFlash(c))

	if username := webcontext.Username(c); username != "" {
		ginertia.ShareProp(c, "_auth", map[string]any{
			"username": username,
		})
	}
}
