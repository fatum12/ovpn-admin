package web

import (
	"bitbucket.org/fatum12/ginx"
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/validation"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/webcontext"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func ErrorHandler(c *gin.Context) {
	c.Next()

	ginError := c.Errors.Last()
	if ginError == nil {
		return
	}

	switch err := ginError.Err.(type) {
	case *errs.Error:
		c.JSON(err.Status, responseFromError(err))
	case validator.ValidationErrors:
		errsData := validation.FormatErrors(err)
		webcontext.Logger(c).Errorf("validation errors: %+v", errsData)
		webcontext.SetFormErrors(c, errsData)
		ginx.RedirectBack(c)
	default:
		webcontext.Logger(c).Errorf("%+v", err)
		appErr := errs.New(errs.InternalError).WithCause(err)
		c.JSON(appErr.Status, responseFromError(appErr))
	}
}

func responseFromError(err *errs.Error) *ErrorResponse {
	resp := ErrorResponse{
		Code:    err.Code,
		Message: err.Message,
	}
	if err.Cause != nil {
		resp.Detail = err.Cause.Error()
	}
	if len(err.Data) != 0 {
		resp.Data = err.Data
	}
	return &resp
}
