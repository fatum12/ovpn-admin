package web

import (
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"github.com/gin-gonic/gin"
	"runtime/debug"
)

func Recovery(c *gin.Context) {
	defer func() {
		if rval := recover(); rval != nil {
			logger := logging.CtxLogger(c.Request.Context())
			logger.Errorf("panic: %+v\n%s", rval, debug.Stack())

			err := errs.New(errs.InternalError)
			c.JSON(err.Status, responseFromError(err))
			c.Abort()
		}
	}()

	c.Next()
}
