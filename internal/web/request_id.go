package web

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

func RequestID(logger logrus.FieldLogger) gin.HandlerFunc {
	return func(c *gin.Context) {
		reqID := uuid.NewString()
		ctx := logging.WithLogger(c.Request.Context(), logger.WithField(logging.FieldRequestID, reqID))
		c.Request = c.Request.WithContext(ctx)
		c.Header("X-Request-Id", reqID)
	}
}
