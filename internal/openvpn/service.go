package openvpn

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/config"
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"bitbucket.org/fatum12/ovpn-admin/internal/telnet"
	"bitbucket.org/fatum12/ovpn-admin/internal/utils"
	"context"
	"github.com/EvilSuperstars/go-cidrman"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io"
	"os"
	"path/filepath"
	"text/template"
	"time"
)

const DefaultRoutesConfigName = "_common"
const DefaultClientConfigName = "DEFAULT"

type Service struct {
	cfg          config.OpenVPN
	dnsCfg       config.DNSProxy
	telnetClient *telnet.Client
}

func NewService(cfg config.OpenVPN, dnsCfg config.DNSProxy) *Service {
	return &Service{
		cfg:    cfg,
		dnsCfg: dnsCfg,
	}
}

// Создает поверхностную копию.
func (s *Service) clone() *Service {
	clone := *s
	return &clone
}

func (s *Service) UpdateRoutesConfig(ctx context.Context, name string, routes []*models.Route) error {
	cfg := NewConfigBuilder()
	// сохраняем в конфиг только уникальные ip адреса и сети
	duplicated := map[string]bool{}
	logger := logging.CtxLogger(ctx)

	cfg.Comment("DNS mapping")
	err := writeRoute(s.dnsCfg.MappingRange, cfg, duplicated, logger)
	if err != nil {
		return err
	}

	// ip
	cfg.WriteLine("")
	cfg.Comment("custom ip")
	for _, route := range routes {
		if !route.Type.IsIP() {
			continue
		}
		err = writeRoute(*route.IP, cfg, duplicated, logger)
		if err != nil {
			return err
		}
	}

	// ASN
	cfg.WriteLine("")
	for _, route := range routes {
		if !route.Type.IsASN() {
			continue
		}
		asn := *route.ASN
		ctxTimeout, cancel := context.WithTimeout(ctx, 30*time.Second)
		prefixes, err := utils.ASNPrefixes(ctxTimeout, asn)
		cancel()
		if err != nil {
			return err
		}
		logger.Debugf("%s: %d prefixes", asn, len(prefixes))
		prefixes, err = cidrman.MergeCIDRs(prefixes)
		if err != nil {
			return err
		}
		logger.Debugf("%s: %d merged prefixes", asn, len(prefixes))
		cfg.Comment(asn)
		for _, p := range prefixes {
			err = writeRoute(p, cfg, duplicated, logger)
			if err != nil {
				return err
			}
		}
	}

	return utils.WriteFile(s.routesConfigPath(name), cfg.Bytes())
}

func writeRoute(ipStr string, cfg *ConfigBuilder, duplicated map[string]bool, logger logrus.FieldLogger) error {
	ipStr = utils.NormalizeSubnet(ipStr)
	if duplicated[ipStr] {
		logger.Info("duplicated ip ", ipStr)
		return nil
	}
	ip, mask, err := utils.ParseSubnet(ipStr)
	if err != nil {
		return err
	}
	cfg.PushRoute(ip.String(), mask.String())
	duplicated[ipStr] = true
	return nil
}

func (s *Service) routesConfigPath(name string) string {
	return filepath.Join(s.cfg.ConfigsDir, "routes", name+".conf")
}

func (s *Service) ensureRoutesConfigExists(name string) (err error) {
	path := s.routesConfigPath(name)
	if !utils.FileExists(path) {
		err = s.UpdateRoutesConfig(context.Background(), name, nil)
	}
	return
}

func (s *Service) CommonRoutesConfig() ([]byte, error) {
	data, err := os.ReadFile(s.routesConfigPath(DefaultRoutesConfigName))
	if errors.Is(err, os.ErrNotExist) {
		return nil, errs.New(errs.ConfigNotExists)
	}
	return data, errors.Wrap(err, "read routes config")
}

var clientConfigTmpl = template.Must(template.New("config").Parse(`##### NOTE TO LINUX USERS #####
# OpenVPN does not handle DNS on Linux.
# This also applies to ROUTERS.
#
# You have two workarounds:
# 1. Configure OpenVPN connection using NetworkManager.
#    This is preferrable method.
# 2. Uncomment the lines below
#    For Debian, Ubuntu and derivatives:
#
# script-security 2
# up /etc/openvpn/update-resolv-conf
# down /etc/openvpn/update-resolv-conf
#
#    For Fedora:
#
# script-security 2
# up /usr/share/doc/openvpn/contrib/pull-resolv-conf/client.up
# down /usr/share/doc/openvpn/contrib/pull-resolv-conf/client.down
#
#
# For routers, contact your router manufacturer.
#
###############################

client

dev tun
proto udp
remote {{.config.Host}} {{.config.Port}}
resolv-retry infinite
nobind
persist-tun
persist-key
{{if .config.Cipher -}}
cipher {{.config.Cipher}}
{{ end -}}
{{if .config.Auth -}}
auth {{.config.Auth}}
{{ end -}}
remote-cert-tls server
verb 3

;pull-filter ignore "block-outside-dns"

<ca>
{{.ca | printf "%s"}}
</ca>

<cert>
{{.cert | printf "%s"}}
</cert>

<key>
{{.key | printf "%s"}}
</key>
`))

func (s *Service) WriteClientConnectConfig(ca, cert, key []byte, target io.Writer) error {
	return clientConfigTmpl.Execute(target, map[string]interface{}{
		"config": s.cfg,
		"ca":     ca,
		"cert":   cert,
		"key":    key,
	})
}

func (s *Service) UpdateDefaultClientConfig() error {
	var cfg ConfigBuilder
	cfg.CreatedHeader()

	if err := s.ensureRoutesConfigExists(DefaultRoutesConfigName); err != nil {
		return err
	}
	cfg.Config(s.routesConfigPath(DefaultRoutesConfigName))
	cfg.PushDNS(s.dnsCfg.IP.String())
	cfg.PushBlockOutsideDNS()

	return utils.WriteFile(s.defaultClientConfigPath(), cfg.Bytes())
}

func (s *Service) defaultClientConfigPath() string {
	return s.clientConfigPath(DefaultClientConfigName)
}

func (s *Service) clientConfigPath(name string) string {
	return filepath.Join(s.cfg.ConfigsDir, "ccd", name)
}

func (s *Service) UpdateClientConfig(client *models.Client) error {
	var cfg ConfigBuilder
	cfg.CreatedHeader()

	if client.RoutingAllTraffic {
		cfg.PushRedirectGateway()
	} else {
		if err := s.ensureRoutesConfigExists(DefaultRoutesConfigName); err != nil {
			return err
		}
		cfg.Config(s.routesConfigPath(DefaultRoutesConfigName))
	}
	if client.PrimaryDNS != "" {
		cfg.PushDNS(client.PrimaryDNS)
		if client.SecondaryDNS != "" {
			cfg.PushDNS(client.SecondaryDNS)
		}
	} else {
		cfg.PushDNS(s.dnsCfg.IP.String())
	}
	cfg.PushBlockOutsideDNS()

	return utils.WriteFile(s.clientConfigPath(client.Name), cfg.Bytes())
}

func (s *Service) ClientConfig(clientName string) ([]byte, error) {
	data, err := os.ReadFile(s.clientConfigPath(clientName))
	if errors.Is(err, os.ErrNotExist) {
		return nil, errs.New(errs.ConfigNotExists)
	}
	return data, errors.Wrap(err, "read client config")
}
