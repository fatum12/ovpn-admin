package openvpn

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"bitbucket.org/fatum12/ovpn-admin/internal/telnet"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"strconv"
	"strings"
	"time"
)

type Connection struct {
	Name           string    `json:"name"`
	RealAddress    string    `json:"realAddress"`
	VirtualAddress string    `json:"virtualAddress"`
	BytesReceived  int       `json:"bytesReceived"`
	BytesSent      int       `json:"bytesSent"`
	ConnectedDate  time.Time `json:"connectedDate"`
}

func (s *Service) connectTelnet(ctx context.Context) (*telnet.Client, error) {
	addr := fmt.Sprintf("%s:%d", s.cfg.Telnet.Host, s.cfg.Telnet.Port)
	logging.CtxLogger(ctx).Debug("connecting to telnet ", addr)
	t, err := telnet.Dial(addr, telnet.Options{
		ConnectTimeout: 5 * time.Second,
		Timeout:        2 * time.Second,
	})
	if err != nil {
		return nil, err
	}
	// читаем приглашение
	_, err = t.ReadUntil("\r\n")
	return t, err
}

func (s *Service) doTelnet(ctx context.Context, f func(*telnet.Client) error) error {
	if s.telnetClient != nil {
		return f(s.telnetClient)
	}
	telnetClient, err := s.connectTelnet(ctx)
	if err != nil {
		return err
	}
	defer func() { _ = telnetClient.Close() }()
	return f(telnetClient)
}

// WithTelnetConnection выполняет telnet команды в одном сетевом соединении.
func (s *Service) WithTelnetConnection(ctx context.Context, f func(*Service) error) (err error) {
	if s.telnetClient != nil {
		return f(s)
	}
	clone := s.clone()
	clone.telnetClient, err = clone.connectTelnet(ctx)
	if err != nil {
		return err
	}
	defer func() { _ = clone.telnetClient.Close() }()
	return f(clone)
}

func (s *Service) executeTelnetCommand(ctx context.Context, cmd, prompt string) (res []byte, err error) {
	logger := logging.CtxLogger(ctx)
	logger.Debugf("exec telnet command: %s", cmd)
	err = s.doTelnet(ctx, func(t *telnet.Client) error {
		_, e := t.Write([]byte(cmd + "\n"))
		if e != nil {
			return e
		}
		res, e = t.ReadUntil(prompt)
		logger.Debugf("telnet response: %s", res)
		return e
	})
	return res, err
}

func (s *Service) Connections(ctx context.Context) ([]*Connection, error) {
	output, err := s.executeTelnetCommand(ctx, "status 2", "END\r\n")
	if err != nil {
		return nil, err
	}
	result := make([]*Connection, 0)
	for _, line := range strings.Split(string(output), "\n") {
		line = strings.TrimSpace(line)
		if !strings.HasPrefix(line, "CLIENT_LIST") {
			continue
		}
		parts := strings.Split(line, ",")
		var conn *Connection
		if len(parts) > 9 {
			conn, err = buildConnection(parts)
		} else {
			conn, err = buildConnectionLegacy(parts)
		}
		if err != nil {
			return nil, err
		}
		result = append(result, conn)
	}
	return result, nil
}

func (s *Service) KillConnection(ctx context.Context, name string) error {
	_, err := s.executeTelnetCommand(ctx, "kill "+name, "\n")
	return err
}

func (s *Service) Version(ctx context.Context) (string, error) {
	output, err := s.executeTelnetCommand(ctx, "version", "END\r\n")
	if err != nil {
		return "", err
	}
	str := string(output)
	from := strings.IndexByte(str, ':')
	if from < 0 {
		return "", nil
	}
	to := strings.IndexByte(str, '\n')
	if to < 0 || to < from {
		return "", nil
	}
	return strings.TrimSpace(str[from+1 : to]), nil
}

type Stats struct {
	ClientsCount int `json:"clientsCount"`
	BytesIn      int `json:"bytesIn"`
	BytesOut     int `json:"bytesOut"`
}

func (s *Service) Stats(ctx context.Context) (res Stats, err error) {
	output, err := s.executeTelnetCommand(ctx, "load-stats", "\n")
	if err != nil {
		return
	}
	parts := strings.SplitN(string(output), ":", 2)
	if parts[0] != "SUCCESS" || len(parts) != 2 {
		err = errors.Errorf("invalid load-stats result: %s", output)
		return
	}
	for _, param := range strings.Split(strings.TrimSpace(parts[1]), ",") {
		parts = strings.SplitN(param, "=", 2)
		if len(parts) != 2 {
			err = errors.Errorf("invalid stats param: %s", param)
			return
		}
		switch parts[0] {
		case "nclients":
			res.ClientsCount, err = strconv.Atoi(parts[1])
		case "bytesin":
			res.BytesIn, err = strconv.Atoi(parts[1])
		case "bytesout":
			res.BytesOut, err = strconv.Atoi(parts[1])
		default:
			err = errors.Errorf("unknown param: %s", parts[0])
		}
		if err != nil {
			return
		}
	}
	return
}

// CLIENT_LIST,Common Name,Real Address,Virtual Address,Virtual IPv6 Address,Bytes Received,Bytes Sent,Connected Since,Connected Since (time_t),Username,Client ID,Peer ID
func buildConnection(parts []string) (*Connection, error) {
	conn := Connection{
		Name:           parts[1],
		RealAddress:    parts[2],
		VirtualAddress: parts[3],
		BytesReceived:  0,
		BytesSent:      0,
		ConnectedDate:  time.Time{},
	}
	var err error
	conn.BytesReceived, err = strconv.Atoi(parts[5])
	if err != nil {
		return nil, err
	}
	conn.BytesSent, err = strconv.Atoi(parts[6])
	if err != nil {
		return nil, err
	}
	conn.ConnectedDate, err = unixStrToTime(parts[8])

	return &conn, err
}

// CLIENT_LIST,Common Name,Real Address,Virtual Address,Bytes Received,Bytes Sent,Connected Since,Connected Since (time_t),Username
func buildConnectionLegacy(parts []string) (*Connection, error) {
	conn := Connection{
		Name:           parts[1],
		RealAddress:    parts[2],
		VirtualAddress: parts[3],
		BytesReceived:  0,
		BytesSent:      0,
		ConnectedDate:  time.Time{},
	}
	var err error
	conn.BytesReceived, err = strconv.Atoi(parts[4])
	if err != nil {
		return nil, err
	}
	conn.BytesSent, err = strconv.Atoi(parts[5])
	if err != nil {
		return nil, err
	}
	conn.ConnectedDate, err = unixStrToTime(parts[7])

	return &conn, err
}

func unixStrToTime(unix string) (t time.Time, err error) {
	if unix == "" {
		return
	}
	var i int64
	i, err = strconv.ParseInt(unix, 10, 64)
	if err != nil {
		return
	}
	t = time.Unix(i, 0)
	return
}
