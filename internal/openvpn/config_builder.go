package openvpn

import (
	"bytes"
	"time"
)

type ConfigBuilder struct {
	buf bytes.Buffer
}

func NewConfigBuilder() *ConfigBuilder {
	cfg := &ConfigBuilder{}
	cfg.CreatedHeader()
	return cfg
}

func (cb *ConfigBuilder) WriteLine(str string) {
	cb.buf.WriteString(str)
	cb.buf.WriteByte('\n')
}

func (cb *ConfigBuilder) PushRoute(ip, mask string) {
	if mask != "" && mask != "255.255.255.255" {
		ip += " " + mask
	}
	cb.WriteLine(`push "route ` + ip + `"`)
}

func (cb *ConfigBuilder) PushRedirectGateway() {
	cb.WriteLine(`push "redirect-gateway def1"`)
}

func (cb *ConfigBuilder) PushDNS(dns string) {
	cb.WriteLine(`push "dhcp-option DNS ` + dns + `"`)
}

func (cb *ConfigBuilder) PushBlockOutsideDNS() {
	cb.WriteLine(`push "block-outside-dns"`)
}

func (cb *ConfigBuilder) Comment(comment string) {
	cb.WriteLine("# " + comment)
}

func (cb *ConfigBuilder) CreatedHeader() {
	cb.Comment("Created at " + time.Now().Format(time.RFC3339) + "\n")
}

func (cb *ConfigBuilder) Config(path string) {
	cb.WriteLine("config " + path)
}

func (cb *ConfigBuilder) Bytes() []byte {
	return cb.buf.Bytes()
}
