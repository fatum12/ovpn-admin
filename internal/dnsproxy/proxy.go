package dnsproxy

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/config"
	"bufio"
	"bytes"
	"context"
	"github.com/AdguardTeam/dnsproxy/proxy"
	"github.com/AdguardTeam/dnsproxy/upstream"
	"github.com/miekg/dns"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"net"
	"os/exec"
	"strings"
	"sync"
)

type Proxy struct {
	proxy  *proxy.Proxy
	cfg    config.DNSProxy
	logger logrus.FieldLogger

	// Список доменных имен для проксирования.
	reservedDomains     map[string]bool
	reservedDomainsLock sync.RWMutex

	ipMap      map[string]net.IP
	ipMapLock  sync.Mutex
	ipMapNet   *net.IPNet
	lastUsedIP net.IP
}

func New(cfg config.DNSProxy, logger logrus.FieldLogger) (*Proxy, error) {
	if cfg.Port == 0 {
		cfg.Port = 53
	}
	if cfg.IPTablesPath == "" {
		cfg.IPTablesPath = "iptables"
	}

	p := &Proxy{
		cfg:    cfg,
		logger: logger,
	}

	proxyCfg := &proxy.Config{
		ResponseHandler: p.handleResponse,
		UDPListenAddr:   []*net.UDPAddr{{Port: cfg.Port, IP: cfg.IP}},
		CacheEnabled:    cfg.CacheEnabled,
		CacheSizeBytes:  cfg.CacheSize,
	}

	var err error
	proxyCfg.UpstreamConfig, err = proxy.ParseUpstreamsConfig(cfg.Upstreams, &upstream.Options{
		Timeout: cfg.Timeout,
	})
	if err != nil {
		return nil, errors.Wrap(err, "parse dns upstreams")
	}

	p.proxy, err = proxy.New(proxyCfg)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	_, p.ipMapNet, err = net.ParseCIDR(cfg.MappingRange)
	if err != nil {
		return nil, errors.Wrap(err, "parse dns mapping range")
	}
	p.lastUsedIP = cloneIP(p.ipMapNet.IP)
	p.ipMap = make(map[string]net.IP)

	err = p.readExistsIPTablesRules()
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (p *Proxy) Start(ctx context.Context) error {
	p.logger.Info("starting dns proxy...")
	err := p.proxy.Start(ctx)
	if err != nil {
		return errors.Wrap(err, "start dns proxy")
	}
	p.logger.Info("dns proxy started")
	return nil
}

func (p *Proxy) Shutdown(ctx context.Context) error {
	p.logger.Info("stopping dns proxy...")
	err := p.proxy.Shutdown(ctx)
	if err != nil {
		return errors.Wrap(err, "stop dns proxy")
	}
	p.logger.Info("dns proxy stopped")
	return nil
}

func (p *Proxy) execIPTables(arg ...string) ([]byte, error) {
	cmd := exec.Command(p.cfg.IPTablesPath, arg...)
	p.logger.Debug("exec: ", cmd.String())
	out, err := cmd.CombinedOutput()
	p.logger.Debug("iptables output: ", string(out))
	return out, errors.Wrap(err, "iptables")
}

func (p *Proxy) readExistsIPTablesRules() error {
	out, err := p.execIPTables("-t", "nat", "-nL", p.cfg.IPTablesTableName)
	if err != nil {
		return err
	}
	scanner := bufio.NewScanner(bytes.NewReader(out))
	var n int
	maxIP := p.lastUsedIP
	for scanner.Scan() {
		if n < 2 {
			n++
			continue
		}
		parts := strings.Fields(scanner.Text())
		if len(parts) < 6 {
			continue
		}
		from := strings.TrimPrefix(parts[5], "to:")
		to := net.ParseIP(parts[4])
		if to == nil {
			return errors.Errorf("can't parse iptables ip '%s'", parts[4])
		}
		if to.To4() != nil {
			to = to.To4()
		}
		p.ipMap[from] = to
		if p.ipMapNet.Contains(to) && bytes.Compare(to, maxIP) > 0 {
			maxIP = to
		}
	}
	if err = scanner.Err(); err != nil {
		return errors.WithStack(err)
	}
	p.logger.Debugf("loaded rules: %+v", p.ipMap)
	p.lastUsedIP = cloneIP(maxIP)
	p.logger.Info("last used ip ", p.lastUsedIP)
	return nil
}

func incrementIP(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func cloneIP(ip net.IP) net.IP {
	clone := make(net.IP, len(ip))
	copy(clone, ip)
	return clone
}

func nextIP(ip net.IP) net.IP {
	ip = cloneIP(ip)
	incrementIP(ip)
	return ip
}

func (p *Proxy) handleResponse(dctx *proxy.DNSContext, err error) {
	if err != nil {
		return
	}
	if len(dctx.Req.Question) == 0 ||
		len(dctx.Res.Answer) == 0 ||
		(dctx.Req.Question[0].Qtype != dns.TypeA && dctx.Req.Question[0].Qtype != dns.TypeHTTPS) {
		return
	}
	domain := normalizeDomain(dctx.Req.Question[0].Name)
	if !p.isReservedDomain(domain) {
		return
	}
	for _, a := range dctx.Res.Answer {
		err := p.handleResponseAnswer(domain, a)
		if err != nil {
			p.logger.Error("assign ip: ", err)
			break
		}
	}
}

func (p *Proxy) handleResponseAnswer(domain string, a dns.RR) error {
	switch rec := a.(type) {
	case *dns.A:
		originalIP := rec.A
		fakeIP, err := p.assignFakeIP(originalIP.String())
		if err != nil {
			return err
		}
		rec.A = fakeIP
		p.logger.Debug("domain ", domain, " assign ip ", originalIP, " -> ", fakeIP)
	case *dns.HTTPS:
		for _, val := range rec.Value {
			hint, ok := val.(*dns.SVCBIPv4Hint)
			if !ok {
				continue
			}
			for i, h := range hint.Hint {
				fakeIP, err := p.assignFakeIP(h.String())
				if err != nil {
					return err
				}
				hint.Hint[i] = fakeIP
				p.logger.Debug("domain ", domain, " assign ip ", h, " -> ", fakeIP)
			}
		}
	default:
		return nil
	}
	if p.cfg.MappingMaxTTL > 0 && a.Header().Ttl > p.cfg.MappingMaxTTL {
		a.Header().Ttl = p.cfg.MappingMaxTTL
	}
	return nil
}

func normalizeDomain(d string) string {
	return strings.ToLower(strings.Trim(d, "."))
}

func (p *Proxy) isReservedDomain(d string) bool {
	p.reservedDomainsLock.RLock()
	defer p.reservedDomainsLock.RUnlock()

	for d != "" {
		res := p.reservedDomains[d]
		if res {
			return true
		}
		pos := strings.IndexByte(d, '.')
		if pos < 0 {
			break
		}
		d = d[pos+1:]
	}
	return false
}

func (p *Proxy) assignFakeIP(ip string) (net.IP, error) {
	p.ipMapLock.Lock()
	defer p.ipMapLock.Unlock()

	if mappedIP, ok := p.ipMap[ip]; ok {
		return mappedIP, nil
	}

	fakeIP := nextIP(p.lastUsedIP)
	if !p.ipMapNet.Contains(fakeIP) {
		return nil, errors.New("no ip addresses left")
	}
	incrementIP(p.lastUsedIP)

	_, err := p.execIPTables("-t", "nat", "-A", p.cfg.IPTablesTableName, "-d", fakeIP.String(), "-j", "DNAT", "--to", ip)
	if err != nil {
		return nil, err
	}

	p.ipMap[ip] = fakeIP
	return fakeIP, nil
}

func (p *Proxy) SetReservedDomains(domains []string) {
	p.reservedDomainsLock.Lock()
	defer p.reservedDomainsLock.Unlock()

	p.reservedDomains = make(map[string]bool, len(domains))
	for _, d := range domains {
		p.reservedDomains[d] = true
	}
}

func (p *Proxy) ClearCache() error {
	p.ipMapLock.Lock()
	defer p.ipMapLock.Unlock()

	_, err := p.execIPTables("-t", "nat", "-F", p.cfg.IPTablesTableName)
	if err != nil {
		return err
	}

	p.lastUsedIP = cloneIP(p.ipMapNet.IP)
	p.ipMap = make(map[string]net.IP)

	p.proxy.ClearCache()
	return nil
}
