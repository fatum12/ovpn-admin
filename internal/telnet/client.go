package telnet

import (
	"bufio"
	"bytes"
	"fmt"
	"net"
	"time"
)

const (
	SE   = 240 // Subnegotiation End
	SB   = 250 // Subnegotiation Begin
	WILL = 251
	WONT = 252
	DO   = 253
	DONT = 254
	IAC  = 255 // Interpret As Command
)

type Logger interface {
	Println(...interface{})
}

type nullLogger struct{}

func (*nullLogger) Println(_ ...interface{}) {}

type Options struct {
	ConnectTimeout time.Duration
	Timeout        time.Duration
	Logger         Logger
}

type Client struct {
	conn    net.Conn
	reader  *bufio.Reader
	options Options
}

func Dial(addr string, options Options) (*Client, error) {
	d := net.Dialer{Timeout: options.ConnectTimeout}
	conn, err := d.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}

	if options.Logger == nil {
		options.Logger = &nullLogger{}
	}

	client := Client{
		conn:    conn,
		reader:  bufio.NewReader(conn),
		options: options,
	}
	return &client, nil
}

func (c *Client) Close() error {
	return c.conn.Close()
}

func (c *Client) ReadUntil(prompt string) (data []byte, err error) {
	if c.options.Timeout > 0 {
		if err = c.conn.SetReadDeadline(time.Now().Add(c.options.Timeout)); err != nil {
			return
		}
	}
	promptBytes := []byte(prompt)

	for {
		var b byte
		b, err = c.reader.ReadByte()
		if err != nil {
			return
		}

		if b == IAC {
			var cmd, option byte
			cmd, err = c.reader.ReadByte()
			if err != nil {
				return
			}
			c.options.Logger.Println("read cmd: ", cmd)

			switch cmd {
			case WILL, WONT, DO, DONT:
				option, err = c.reader.ReadByte()
				if err != nil {
					return
				}
				if cmd == DO || cmd == DONT {
					_, err = c.conn.Write([]byte{IAC, WONT, option})
				} else if cmd == WILL || cmd == WONT {
					_, err = c.conn.Write([]byte{IAC, DONT, option})
				}
				if err != nil {
					return
				}
			case IAC:
				data = append(data, IAC)
			case SB:
				for {
					var b2 byte
					b2, err = c.reader.ReadByte()
					if err != nil {
						return
					}

					if b2 == IAC {
						cmd, err = c.reader.ReadByte()
						if err != nil {
							return
						}

						if cmd == SE {
							break
						}
					}
				}
			default:
				return data, fmt.Errorf("unknown control character: %v", cmd)
			}
		} else {
			data = append(data, b)
		}

		if bytes.HasSuffix(data, promptBytes) {
			break
		}
	}
	return
}

var escapedIAC = []byte{IAC, IAC}

func (c *Client) Write(p []byte) (n int, err error) {
	if len(p) == 0 {
		return
	}
	if c.options.Timeout > 0 {
		if err = c.conn.SetWriteDeadline(time.Now().Add(c.options.Timeout)); err != nil {
			return
		}
	}

	var buffer bytes.Buffer
	var written int
	for _, b := range p {
		if b == IAC {
			if buffer.Len() > 0 {
				written, err = c.conn.Write(buffer.Bytes())
				n += written
				if err != nil {
					return
				}
				buffer.Reset()
			}

			_, err = c.conn.Write(escapedIAC)
			if err != nil {
				return
			}
			n++
		} else {
			buffer.WriteByte(b)
		}
	}

	if buffer.Len() > 0 {
		written, err = c.conn.Write(buffer.Bytes())
		n += written
	}
	return
}
