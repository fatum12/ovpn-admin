package logging

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

type Config struct {
	Level string `yaml:"level"`
}

func Setup(logger *logrus.Logger, cfg Config) error {
	logger.SetOutput(os.Stdout)

	lvl, err := logrus.ParseLevel(cfg.Level)
	if err != nil {
		logger.Warn("unsupported log level: ", cfg.Level)
		lvl = logrus.InfoLevel
	}
	logger.SetLevel(lvl)

	logger.SetFormatter(&Formatter{
		FieldsOrder: []string{FieldComponent, FieldRequestID},
		FieldsHideLabel: map[string]bool{
			FieldComponent: true,
			FieldRequestID: true,
		},
	})

	return nil
}

type ctxKey string

const loggerKey ctxKey = "logger"

func WithLogger(ctx context.Context, logger logrus.FieldLogger) context.Context {
	return context.WithValue(ctx, loggerKey, logger)
}

func CtxLogger(ctx context.Context) logrus.FieldLogger {
	logger, _ := ctx.Value(loggerKey).(logrus.FieldLogger)
	if logger == nil {
		logger = logrus.StandardLogger()
	}
	return logger
}

func FormatDuration(d time.Duration) string {
	return fmt.Sprintf("%.3f", d.Seconds())
}
