package logging

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"
)

const timestampFormat = "2006-01-02 15:04:05.000"

const (
	FieldRequestID = "request_id"
	FieldComponent = "component"
)

type Formatter struct {
	FieldsOrder     []string
	FieldsHideLabel map[string]bool
}

func (f *Formatter) Format(entry *logrus.Entry) ([]byte, error) {
	var prefix bytes.Buffer

	// write timestamp
	prefix.WriteString(entry.Time.Format(timestampFormat))
	prefix.WriteByte(' ')

	// write level
	f.writeField(&prefix, "", entry.Level.String())

	// write user fields
	ignoreFields := make(map[string]bool, len(f.FieldsOrder))
	for _, fieldName := range f.FieldsOrder {
		if val, ok := entry.Data[fieldName]; ok {
			f.writeField(&prefix, fieldName, val)
		}
		ignoreFields[fieldName] = true
	}
	for fieldName, val := range entry.Data {
		if !ignoreFields[fieldName] {
			f.writeField(&prefix, fieldName, val)
		}
	}

	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	for _, line := range strings.Split(entry.Message, "\n") {
		b.Write(prefix.Bytes())
		b.WriteString(line)
		b.WriteByte('\n')
	}

	return b.Bytes(), nil
}

func (f *Formatter) writeField(b *bytes.Buffer, name string, val interface{}) {
	b.WriteByte('[')
	if name != "" && !f.FieldsHideLabel[name] {
		b.WriteString(name)
		b.WriteByte(':')
	}
	b.WriteString(formatValue(val))
	b.WriteString("] ")
}

func formatValue(val interface{}) string {
	if str, ok := val.(string); ok {
		return str
	}
	return fmt.Sprintf("%v", val)
}
