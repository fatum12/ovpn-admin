package database

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/config"
	"github.com/pkg/errors"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

// Connect подключается к базе данных.
func Connect(cfg config.Database) (*gorm.DB, error) {
	var dbLogger logger.Interface
	if cfg.Debug {
		dbLogger = logger.New(
			log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
			logger.Config{
				SlowThreshold: time.Second, // Slow SQL threshold
				LogLevel:      logger.Info, // Log level
				Colorful:      true,
			},
		)
	}

	db, err := gorm.Open(sqlite.Open(cfg.Path+"?_foreign_keys=1"), &gorm.Config{
		Logger: dbLogger,
	})
	return db, errors.Wrap(err, "connect to database")
}
