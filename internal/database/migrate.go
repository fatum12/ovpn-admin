package database

import (
	"bitbucket.org/fatum12/ovpn-admin/assets"
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Migrate применяет миграции к базе.
func Migrate(db *sql.DB, logger logrus.FieldLogger) error {
	logger.Info("migrating database")
	m, err := newMigrator(db, logger)
	if err != nil {
		return err
	}
	err = m.Up()
	if errors.Is(err, migrate.ErrNoChange) {
		logger.Info(err)
		return nil
	}
	return errors.WithStack(err)
}

func newMigrator(db *sql.DB, logger logrus.FieldLogger) (*migrate.Migrate, error) {
	driver, err := sqlite3.WithInstance(db, &sqlite3.Config{})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	source, err := iofs.New(assets.Assets, "migrations")
	if err != nil {
		return nil, errors.WithStack(err)
	}

	m, err := migrate.NewWithInstance("iofs", source, "sqlite3", driver)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	m.Log = &migrateLogger{logger}
	return m, nil
}

type migrateLogger struct {
	logrus.FieldLogger
}

func (l *migrateLogger) Verbose() bool {
	return false
}
