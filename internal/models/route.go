package models

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/utils"
	"github.com/pkg/errors"
	"github.com/xissy/lexorank"
	"gorm.io/gorm"
	"time"
)

type RouteType int

const (
	RouteTypeUndefined RouteType = 0
	RouteTypeIP        RouteType = 1
	RouteTypeHost      RouteType = 2
	RouteTypeASN       RouteType = 3
)

func (rt RouteType) IsValid() bool {
	return rt.IsIP() || rt.IsHost() || rt.IsASN()
}

func (rt RouteType) IsIP() bool {
	return rt == RouteTypeIP
}

func (rt RouteType) IsHost() bool {
	return rt == RouteTypeHost
}

func (rt RouteType) IsASN() bool {
	return rt == RouteTypeASN
}

type Route struct {
	ID          int       `json:"id" gorm:"primaryKey"`
	Type        RouteType `json:"type"`
	Deleted     bool      `json:"deleted"`
	Processed   bool      `json:"processed"`
	Created     time.Time `json:"created" gorm:"autoCreateTime"`
	Updated     time.Time `json:"-" gorm:"autoUpdateTime"`
	Rank        string    `json:"-"`
	Description *string   `json:"description,omitempty"`

	IP *string `json:"ip,omitempty"`

	Host *string `json:"host,omitempty"`

	ASN *string `json:"asn,omitempty"`
}

func (*Route) TableName() string {
	return "routes"
}

func (r *Route) BeforeCreate(db *gorm.DB) error {
	if r.Rank == "" {
		var maxRank string
		err := db.Model(&Route{}).Select("ifnull(max(rank), '')").Row().Scan(&maxRank)
		if err != nil {
			return err
		}
		var ok bool
		r.Rank, ok = lexorank.Rank(maxRank, "")
		if !ok {
			return errors.Errorf("lexorank: can't allocate between %q and ''", maxRank)
		}
	}
	return nil
}

func (r *Route) reset() {
	r.Type = RouteTypeUndefined
	r.IP = nil
	r.Host = nil
	r.ASN = nil
}

func (r *Route) SetIP(ip string) {
	r.reset()
	r.Type = RouteTypeIP
	ip = utils.NormalizeSubnet(ip)
	r.IP = &ip
}

func (r *Route) SetHost(host string) {
	r.reset()
	r.Type = RouteTypeHost
	host = utils.NormalizeHost(host)
	r.Host = &host
}

func (r *Route) SetASN(asn string) {
	r.reset()
	r.Type = RouteTypeASN
	asn = utils.NormalizeASN(asn)
	r.ASN = &asn
}
