package models

import "time"

type Client struct {
	ID                int       `json:"id" gorm:"primaryKey"`
	Name              string    `json:"name"`
	RoutingAllTraffic bool      `json:"routingAllTraffic"`
	PrimaryDNS        string    `json:"primaryDns"`
	SecondaryDNS      string    `json:"secondaryDns"`
	Created           time.Time `json:"created" gorm:"autoCreateTime"`
	Updated           time.Time `json:"-" gorm:"autoUpdateTime"`
}

func (*Client) TableName() string {
	return "clients"
}
