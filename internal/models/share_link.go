package models

import (
	"crypto/rand"
	"encoding/base64"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"time"
)

// ShareLink ссылка для импорта клиентского конфига.
type ShareLink struct {
	ID        int        `json:"id" gorm:"column:id;primaryKey"`
	Token     string     `json:"-" gorm:"column:token"` // Токен доступа для подстановки в url
	ClientID  int        `json:"-" gorm:"column:client_id"`
	Created   time.Time  `json:"created" gorm:"column:created;autoCreateTime"`
	ExpireAt  time.Time  `json:"expireAt" gorm:"column:expire_at"`
	Processed *time.Time `json:"processed" gorm:"column:processed"`
}

func (*ShareLink) TableName() string {
	return "share_links"
}

func (sl *ShareLink) BeforeCreate(_ *gorm.DB) error {
	buf := make([]byte, 32)
	_, err := rand.Read(buf)
	if err != nil {
		return errors.WithStack(err)
	}
	sl.Token = base64.RawURLEncoding.EncodeToString(buf)

	sl.ExpireAt = time.Now().Add(24 * time.Hour)
	return nil
}

func (sl *ShareLink) IsExpired() bool {
	return sl.ExpireAt.Before(time.Now())
}

func (sl *ShareLink) IsProcessed() bool {
	return sl.Processed != nil
}
