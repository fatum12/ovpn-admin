package validation

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/di"
	"bitbucket.org/fatum12/ovpn-admin/internal/easyrsa"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"regexp"
)

type Func func(fl validator.FieldLevel, deps *di.Container) bool

func wrapFunc(f Func, deps *di.Container) validator.Func {
	return func(fl validator.FieldLevel) bool {
		return f(fl, deps)
	}
}

type ruleConfig struct {
	Tag     string
	Func    Func
	Message string
}

var rules = []ruleConfig{
	{
		Tag:     "ip|cidr",
		Message: "Invalid ip or subnet",
	},
	{
		Tag:     "cert_name",
		Func:    certificateName,
		Message: "Valid characters: a-z, A-Z, 0-9, -, _",
	},
	{
		Tag:     "unique_cert_name",
		Func:    uniqueCertificateName,
		Message: "The certificate name should be unique",
	},
	{
		Tag:     "unique_route",
		Message: "Route already added",
	},
	{
		Tag:     "route_type",
		Func:    routeType,
		Message: "Invalid route type",
	},
	{
		Tag:     "asn",
		Func:    validASN,
		Message: "Invalid ASN",
	},
	{
		Tag:     "domain_name",
		Func:    validDomainName,
		Message: "Invalid domain name",
	},
}

var rxCertName = regexp.MustCompile(`^[a-zA-Z0-9\-_]+$`)

func certificateName(fl validator.FieldLevel, _ *di.Container) bool {
	value, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	if value == "" {
		return true
	}
	return rxCertName.MatchString(value)
}

func uniqueCertificateName(fl validator.FieldLevel, deps *di.Container) bool {
	value, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	if value == "" {
		return true
	}
	_, err := deps.EasyRSA.CertificateByName(value)
	if errors.Is(err, easyrsa.ErrCertificateNotFound) {
		return true
	}
	if err != nil {
		logrus.Error("validate certificate name: ", err)
	}
	return false
}

func routeType(fl validator.FieldLevel, _ *di.Container) bool {
	value, ok := fl.Field().Interface().(models.RouteType)
	if !ok {
		return false
	}
	return value.IsValid()
}

var rxASN = regexp.MustCompile(`(?i)^AS\d+$`)

func validASN(fl validator.FieldLevel, _ *di.Container) bool {
	value, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	return rxASN.MatchString(value)
}

var rxDomainName = regexp.MustCompile(`(?i)^(https?://)?((xn--)?[a-z\d]+(-[a-z\d]+)*\.)+(xn--)?[a-z\d]{2,}/?$`)

func validDomainName(fl validator.FieldLevel, _ *di.Container) bool {
	value, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	return rxDomainName.MatchString(value)
}
