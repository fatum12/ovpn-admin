package validation

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/di"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	defaultTranslations "github.com/go-playground/validator/v10/translations/en"
	"github.com/pkg/errors"
	"reflect"
	"strings"
)

var translator ut.Translator

// Register добавляет правила валидации.
func Register(v *validator.Validate, deps *di.Container) error {
	v.RegisterTagNameFunc(tagNameFunc)

	enLang := en.New()
	uni := ut.New(enLang, enLang)

	translator, _ = uni.GetTranslator("en")

	err := defaultTranslations.RegisterDefaultTranslations(v, translator)
	if err != nil {
		return errors.WithStack(err)
	}

	for _, rCfg := range rules {
		if rCfg.Func != nil {
			if err = v.RegisterValidation(rCfg.Tag, wrapFunc(rCfg.Func, deps)); err != nil {
				return errors.WithStack(err)
			}
		}

		if rCfg.Message != "" {
			tag := rCfg.Tag

			err = v.RegisterTranslation(
				tag,
				translator,
				func(ut ut.Translator) error {
					return ut.Add(tag, rCfg.Message, false)
				},
				func(ut ut.Translator, fe validator.FieldError) string {
					t, err := ut.T(fe.Tag(), fe.Field())
					if err != nil {
						return fe.(error).Error()
					}
					return t
				},
			)
			if err != nil {
				return errors.WithStack(err)
			}
		}
	}

	return nil
}

type Field struct {
	Path   string       `json:"path"`
	Errors []FieldError `json:"errors"`
}

type FieldError struct {
	Rule    string `json:"rule"`
	Param   string `json:"param,omitempty"`
	Message string `json:"message"`
}

func FormatErrors(valErrs validator.ValidationErrors) map[string]string {
	result := make(map[string]string, len(valErrs))
	for _, err := range valErrs {
		parts := strings.SplitN(err.Namespace(), ".", 2)
		var fieldName string
		if len(parts) > 1 {
			fieldName = parts[1]
		} else {
			fieldName = err.Field()
		}
		if _, ok := result[fieldName]; !ok {
			result[fieldName] = err.Translate(translator)
		}
	}
	return result
}

func tagNameFunc(fld reflect.StructField) string {
	name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
	if name == "-" {
		return ""
	}
	return name
}

func SelfValidator(deps *di.Container) validator.StructLevelFunc {
	return func(sl validator.StructLevel) {
		obj := sl.Current().Interface().(selfValidatable)
		obj.Validate(sl, deps)
	}
}

type selfValidatable interface {
	Validate(sl validator.StructLevel, deps *di.Container)
}
