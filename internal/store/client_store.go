package store

import (
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"context"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

func (s *Store) GetClients() ([]*models.Client, error) {
	var clients []*models.Client
	err := s.db.Order("id").Find(&clients).Error
	return clients, errors.Wrap(err, "get clients")
}

func (s *Store) CreateClient(c *models.Client) error {
	err := s.db.Create(c).Error
	return errors.Wrap(err, "create client")
}

func (s *Store) SaveClient(c *models.Client) error {
	err := s.db.Save(c).Error
	return errors.Wrap(err, "save client")
}

func (s *Store) GetClientByName(name string) (*models.Client, error) {
	var c models.Client
	err := s.db.Take(&c, "name = ?", name).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, errs.New(errs.EntityNotFound)
	}
	return &c, errors.Wrap(err, "get client by name")
}

func (s *Store) GetClientByID(ctx context.Context, id int) (*models.Client, error) {
	c := &models.Client{}
	err := s.db.WithContext(ctx).Take(c, id).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, errs.New(errs.EntityNotFound)
	}
	return c, errors.Wrap(err, "get client by id")
}

func (s *Store) DeleteAllClients() error {
	err := s.db.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(&models.Client{}).Error
	return errors.Wrap(err, "delete all clients")
}
