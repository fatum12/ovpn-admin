package store

import (
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"github.com/pkg/errors"
	"github.com/xissy/lexorank"
	"gorm.io/gorm"
)

func DeletedRoutes(v bool) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("deleted = ?", v)
	}
}

func ProcessedRoutes(v bool) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("processed = ?", v)
	}
}

func RouteIP(ip string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("ip = ?", ip)
	}
}

func RouteHost(host string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("host = ?", host)
	}
}

func RouteASN(asn string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("asn = ?", asn)
	}
}

func RouteType(t models.RouteType) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where("type = ?", t)
	}
}

func (s *Store) GetRoutes(conditions ...func(db *gorm.DB) *gorm.DB) ([]*models.Route, error) {
	db := s.db.Scopes(conditions...)

	routes := make([]*models.Route, 0)
	err := db.Order("rank, id").
		Find(&routes).
		Error
	return routes, errors.Wrap(err, "get routes")
}

func (s *Store) GetRoute(conditions ...func(db *gorm.DB) *gorm.DB) (*models.Route, error) {
	db := s.db.Scopes(conditions...)
	var route models.Route
	err := db.Take(&route).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, errs.New(errs.EntityNotFound)
	}
	return &route, errors.Wrap(err, "get route")
}

func (s *Store) CreateRoute(route *models.Route) error {
	err := s.db.Create(route).Error
	return errors.Wrap(err, "create route")
}

func (s *Store) SoftDeleteRoute(id int) error {
	err := s.db.Model(&models.Route{ID: id}).Update("deleted", true).Error
	return errors.Wrap(err, "soft delete route")
}

func (s *Store) RestoreRoute(id int) error {
	err := s.db.Model(&models.Route{ID: id}).Update("deleted", false).Error
	return errors.Wrap(err, "restore route")
}

func (s *Store) DeleteMarkedRoutes() error {
	err := s.db.Where("deleted = true").Delete(&models.Route{}).Error
	return errors.WithStack(err)
}

func (s *Store) MarkRoutesAsProcessed() error {
	err := s.db.Model(&models.Route{}).
		Where("processed = false").
		Where("deleted = false").
		Update("processed", true).
		Error
	return errors.WithStack(err)
}

func (s *Store) GetRouteByID(id int) (*models.Route, error) {
	var route models.Route
	err := s.db.Take(&route, id).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, errs.New(errs.EntityNotFound)
	}
	if err != nil {
		return nil, errors.Wrap(err, "get route by id")
	}
	return &route, nil
}

func (s *Store) ReorderRoute(id, prevID, nextID int) error {
	var prevRank, nextRank string
	if prevID > 0 {
		prevRoute, err := s.GetRouteByID(prevID)
		if err != nil {
			return err
		}
		prevRank = prevRoute.Rank
	}
	if nextID > 0 {
		nextRoute, err := s.GetRouteByID(nextID)
		if err != nil {
			return err
		}
		nextRank = nextRoute.Rank
	}

	rank, _ := lexorank.Rank(prevRank, nextRank)
	err := s.db.Model(&models.Route{ID: id}).Update("rank", rank).Error
	return errors.Wrap(err, "reorder route")
}
