package store

import (
	errs "bitbucket.org/fatum12/ovpn-admin/internal/errors"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"context"
	"github.com/AlekSi/pointer"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"time"
)

func (s *Store) CreateShareLink(ctx context.Context, link *models.ShareLink) error {
	err := s.db.WithContext(ctx).Create(link).Error
	return errors.Wrap(err, "create share link")
}

func (s *Store) GetShareLinkByID(ctx context.Context, id int) (*models.ShareLink, error) {
	res := &models.ShareLink{}
	err := s.db.WithContext(ctx).Take(res, id).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, errs.New(errs.EntityNotFound)
	}
	return res, errors.Wrap(err, "get share link by id")
}

func (s *Store) GetShareLinkByToken(ctx context.Context, token string) (*models.ShareLink, error) {
	res := &models.ShareLink{}
	err := s.db.WithContext(ctx).Take(res, "token = ?", token).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, errs.New(errs.EntityNotFound)
	}
	return res, errors.Wrap(err, "get share link by token")
}

func (s *Store) DeleteShareLink(ctx context.Context, id int) error {
	err := s.db.WithContext(ctx).Delete(&models.ShareLink{}, id).Error
	return errors.Wrap(err, "delete share link")
}

func (s *Store) MarkShareLinkAsProcessed(ctx context.Context, id int) error {
	err := s.db.WithContext(ctx).Updates(&models.ShareLink{
		ID:        id,
		Processed: pointer.ToTime(time.Now()),
	}).Error
	return errors.Wrap(err, "mark share link as processed")
}

func (s *Store) DeleteExpiredShareLinks(ctx context.Context) (int64, error) {
	res := s.db.WithContext(ctx).Delete(&models.ShareLink{}, "expire_at < ?", time.Now())
	return res.RowsAffected, errors.Wrap(res.Error, "delete all expired links")
}
