package store

import "gorm.io/gorm"

type Store struct {
	db *gorm.DB
}

func New(db *gorm.DB) *Store {
	return &Store{
		db: db,
	}
}
