package di

import (
	"bitbucket.org/fatum12/ovpn-admin/internal/config"
	"bitbucket.org/fatum12/ovpn-admin/internal/dnsproxy"
	"bitbucket.org/fatum12/ovpn-admin/internal/easyrsa"
	"bitbucket.org/fatum12/ovpn-admin/internal/netmetrics"
	"bitbucket.org/fatum12/ovpn-admin/internal/openvpn"
	"bitbucket.org/fatum12/ovpn-admin/internal/store"
	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type Container struct {
	Config     config.Config
	Logger     logrus.FieldLogger
	Validator  *validator.Validate
	DB         *gorm.DB
	Store      *store.Store
	OpenVPN    *openvpn.Service
	EasyRSA    *easyrsa.Client
	NetMetrics *netmetrics.Stat
	DNSProxy   *dnsproxy.Proxy
}
