import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { fileURLToPath, URL } from 'node:url'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
      vue({
        template: {
          compilerOptions: {
            whitespace: 'preserve',
          }
        }
      })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    outDir: '../assets/frontend',
    manifest: true,
    rollupOptions: {
      input: "src/main.js",
    },
  },
})
