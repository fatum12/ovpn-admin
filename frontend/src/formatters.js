const units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

export function bytes(value) {
    let num = parseInt(value, 10);

    if (num < 1000) {
        return num + ' B';
    }

    const pos = Math.floor(Math.log(num) / Math.log(1000));

    return roundDecimal(num / Math.pow(1000, pos)) + ' ' + units[pos];
}

function roundDecimal(value) {
    return Math.round(value * 100) / 100;
}

const dateTimeOptions = {
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric'
};

export function dateTime(value) {
    if (!value) {
        return '';
    }
    return new Intl.DateTimeFormat(navigator.language, dateTimeOptions).format(new Date(value));
}

export function duration(value) {
    let diff = new Date() - new Date(value);
    diff = Math.floor(diff / 1000); // seconds

    const days = Math.floor(diff / 3600 / 24);
    diff -= days * 3600 * 24;
    const hours = Math.floor(diff / 3600);
    diff -= hours * 3600;
    const minutes = Math.floor(diff / 60);
    diff -= minutes * 60;

    let res = '';
    if (days > 0) {
        res += days + 'd ';
    }
    res += `${padding(hours)}:${padding(minutes)}:${padding(diff)}`;
    return res;
}

function padding(value) {
    return value < 10 ? '0' + value : value;
}
