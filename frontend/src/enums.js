export const routeType = Object.freeze({
    IP: 1,
    HOST: 2,
    ASN: 3,
});
