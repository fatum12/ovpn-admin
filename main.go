package main

import (
	"bitbucket.org/fatum12/ginertia"
	"bitbucket.org/fatum12/ginx/sessions"
	"bitbucket.org/fatum12/ovpn-admin/assets"
	"bitbucket.org/fatum12/ovpn-admin/internal/config"
	"bitbucket.org/fatum12/ovpn-admin/internal/database"
	"bitbucket.org/fatum12/ovpn-admin/internal/di"
	"bitbucket.org/fatum12/ovpn-admin/internal/dnsproxy"
	"bitbucket.org/fatum12/ovpn-admin/internal/easyrsa"
	"bitbucket.org/fatum12/ovpn-admin/internal/logging"
	"bitbucket.org/fatum12/ovpn-admin/internal/models"
	"bitbucket.org/fatum12/ovpn-admin/internal/netmetrics"
	"bitbucket.org/fatum12/ovpn-admin/internal/openvpn"
	"bitbucket.org/fatum12/ovpn-admin/internal/scheduler"
	"bitbucket.org/fatum12/ovpn-admin/internal/store"
	"bitbucket.org/fatum12/ovpn-admin/internal/utils"
	"bitbucket.org/fatum12/ovpn-admin/internal/validation"
	"bitbucket.org/fatum12/ovpn-admin/internal/web"
	"bitbucket.org/fatum12/ovpn-admin/internal/web/handlers"
	"context"
	"flag"
	"github.com/alexedwards/scs/gormstore"
	"github.com/alexedwards/scs/v2"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/olivere/vite"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"html/template"
	"io/fs"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Version версия приложения. Задается через ldflags при компиляции.
var Version string

func main() {
	if err := run(); err != nil {
		logrus.Fatal(err)
	}
}

func run() error {
	logger := logrus.StandardLogger()

	cfgPath := flag.String("config", "config.yaml", "path to configuration file")
	doInit := flag.Bool("init", false, "initialize app")
	flag.Parse()

	validateEngine := binding.Validator.Engine().(*validator.Validate)
	cfg, err := config.Load(*cfgPath, validateEngine)
	if err != nil {
		return err
	}

	if err = logging.Setup(logger, cfg.Log); err != nil {
		return err
	}

	db, err := database.Connect(cfg.DB)
	if err != nil {
		return err
	}

	deps := &di.Container{
		Config:    cfg,
		Logger:    logger,
		Validator: validateEngine,
		DB:        db,
		Store:     store.New(db),
		OpenVPN:   openvpn.NewService(cfg.OpenVPN, cfg.DNS),
		EasyRSA:   easyrsa.NewClient(cfg.EasyRSA),
	}

	if err = validation.Register(deps.Validator, deps); err != nil {
		return err
	}

	deps.NetMetrics = netmetrics.NewStat(func() (p netmetrics.Point, err error) {
		res, err := deps.OpenVPN.Stats(context.Background())
		if err != nil {
			return
		}
		p.Timestamp = time.Now().Unix()
		p.RX = res.BytesIn
		p.TX = res.BytesOut
		return
	}, 100)

	if err = migrateDB(deps); err != nil {
		return err
	}

	if *doInit {
		return initialize(deps)
	}

	runner := scheduler.New(logger.WithField(logging.FieldComponent, "cron"))
	if cfg.OpenVPN.TrafficUpdateSchedule != "" {
		err = runner.AddFunc("update_traffic", cfg.OpenVPN.TrafficUpdateSchedule, deps.NetMetrics.Update)
		if err != nil {
			return err
		}
	}
	// каждый день в 00:01
	err = runner.AddFunc("delete_expired_share_links", "1 0 * * *", func() error {
		affected, e := deps.Store.DeleteExpiredShareLinks(context.Background())
		if e == nil {
			logger.Info("expired share links deleted: ", affected)
		}
		return e
	})
	if err != nil {
		return err
	}
	runner.Start()

	dnsProxy, err := dnsproxy.New(cfg.DNS, logger.WithField(logging.FieldComponent, "dns"))
	if err != nil {
		return err
	}
	deps.DNSProxy = dnsProxy
	routes, err := deps.Store.GetRoutes(store.RouteType(models.RouteTypeHost), store.ProcessedRoutes(true))
	if err != nil {
		return err
	}
	if len(routes) > 0 {
		domains := make([]string, len(routes))
		for i, r := range routes {
			domains[i] = *r.Host
		}
		dnsProxy.SetReservedDomains(domains)
	}
	err = dnsProxy.Start(context.Background())
	if err != nil {
		return err
	}

	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.New()
	router.Use(
		web.Recovery,
	)

	templateFuncs := template.FuncMap{
		"json": ginertia.ToJSON,
	}
	var assetsFS fs.FS
	if cfg.Debug {
		// в debug режиме грузим файлы напрямую с файловой системы
		router.SetFuncMap(templateFuncs)
		router.LoadHTMLFiles("assets/index.gohtml")
		assetsFS = os.DirFS("assets")
	} else {
		// в prod режиме используем embed
		templ := template.Must(template.New("").Funcs(templateFuncs).ParseFS(assets.Assets, "index.gohtml"))
		router.SetHTMLTemplate(templ)
		assetsFS = assets.Assets
	}

	httpFS := http.FS(assetsFS)
	router.StaticFileFS("/favicon.ico", "favicon.ico", httpFS)
	router.StaticFileFS("/robots.txt", "robots.txt", httpFS)
	router.StaticFS("/assets", utils.RelativeFS(httpFS, "frontend/assets"))

	frontendFS, err := fs.Sub(assetsFS, "frontend")
	if err != nil {
		return errors.WithStack(err)
	}
	inertia := ginertia.New("index.gohtml")
	inertia.GlobalProps["_version"] = Version
	inertia.TemplateData["vite"], err = vite.HTMLFragment(vite.Config{
		FS:           frontendFS,
		IsDev:        cfg.Debug,
		ViteEntry:    "src/main.js",
		ViteURL:      "http://localhost:5173",
		ViteTemplate: vite.Vue,
	})
	if err != nil {
		return errors.WithStack(err)
	}

	sessionManager := scs.New()
	sessionManager.Lifetime = 24 * time.Hour
	sessionManager.Cookie.Name = "ovpna_session"
	sessionManager.Cookie.HttpOnly = true
	sessionManager.Cookie.Persist = true
	sessionManager.Store, err = gormstore.New(db)
	if err != nil {
		return errors.WithStack(err)
	}

	handlersGroup := router.Group(
		"",
		web.RequestID(logger),
		web.AccessLogger,
		web.ErrorHandler,
		sessions.LoadAndSave(sessionManager),
		inertia.Middleware(),
		web.ShareProps,
	)
	handlers.Register(handlersGroup, deps)

	server := &http.Server{
		Addr:    cfg.Addr,
		Handler: router,
	}
	logger.Info("listening on ", server.Addr)
	go func() {
		if e := server.ListenAndServe(); e != nil && !errors.Is(e, http.ErrServerClosed) {
			logger.Panic(e)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	sig := <-quit
	logger.Infof("got signal: %s, shutting down server...", sig)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	utils.WaitFn(
		func() { runner.Stop(ctx) },
		func() {
			if e := dnsProxy.Shutdown(ctx); e != nil {
				logger.Error(err)
			}
		},
		func() {
			if e := server.Shutdown(ctx); e != nil {
				logger.Error("server forced to shutdown: ", e)
			}
		},
	)

	logger.Info("server exited")
	return nil
}

func migrateDB(deps *di.Container) error {
	logger := deps.Logger.WithField(logging.FieldComponent, "migrator")
	db, err := deps.DB.DB()
	if err != nil {
		return errors.WithStack(err)
	}
	return database.Migrate(db, logger)
}

func initialize(deps *di.Container) error {
	logger := deps.Logger

	logger.Info("updating default client config")
	err := deps.OpenVPN.UpdateDefaultClientConfig()
	if err != nil {
		return err
	}

	logger.Info("refreshing clients table")
	err = deps.Store.DeleteAllClients()
	if err != nil {
		return err
	}

	certs, err := deps.EasyRSA.Certificates()
	if err != nil {
		return err
	}
	for _, cert := range certs {
		client := &models.Client{
			Name: cert.Name,
		}
		if err = deps.Store.CreateClient(client); err != nil {
			return err
		}
		logger.Infof("client '%s' created", cert.Name)
	}

	return nil
}
