package assets

import (
	"embed"
)

//go:embed frontend/* migrations index.gohtml favicon.ico robots.txt
var Assets embed.FS
