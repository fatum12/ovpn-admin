create table if not exists share_links (
    id integer not null primary key,
    token text not null,
    client_id integer not null references clients (id) on delete cascade,
    created datetime not null,
    expire_at datetime not null,
    processed datetime
);

create unique index share_links_token_key on share_links (token);
