create table if not exists clients (
    id integer not null primary key,
    name text not null,
    routing_all_traffic boolean not null default 0,
    primary_dns text not null default '',
    secondary_dns text not null default '',
    created datetime not null,
    updated datetime not null
);

create unique index clients_name_key on clients (name);
