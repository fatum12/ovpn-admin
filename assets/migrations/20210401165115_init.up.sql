create table if not exists routes (
    id integer primary key,
    deleted boolean not null default 0,
    processed boolean not null default 0,
    type integer not null,
    ip text,
    description text,
    host text,
    rank text not null,
    created datetime not null,
    updated datetime not null
);

create unique index routes_ip_key on routes (ip);
create unique index routes_host_key on routes (host);
